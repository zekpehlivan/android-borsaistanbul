


-ignorewarnings
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-dump class_files.txt
-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-allowaccessmodification
-keepattributes *Annotation*
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-repackageclasses ''

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class **.R$* {
  public static <fields>;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep public class * {
    public protected *;
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }

-dontwarn android.support.**
-dontwarn com.google.ads.**

-keepattributes Signature

-keepattributes *Annotation*

-keep class sun.misc.Unsafe { *; }


-keepattributes Signature
-keep class android.webkit.WebViewClient
-keep class * extends android.webkit.WebViewClient
-keepclassmembers class * extends android.webkit.WebViewClient {
    <methods>;
}


 -keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }


 -keep class butterknife.*
 -keepclasseswithmembernames class * { @butterknife.* <methods>; }
 -keepclasseswithmembernames class * { @butterknife.* <fields>; }

 -keep public class org.simpleframework.**{ *; }
 -keep class org.simpleframework.xml.**{ *; }
 -keep class org.simpleframework.xml.core.**{ *; }
 -keep class org.simpleframework.xml.util.**{ *; }
 -keep class com.kaopiz.*
 -keep class com.chauthai.**
 -keep class com.eclipsesource.**
 -keep class com.loopj.**
 -keep class com.github.**
 -keep class com.readystatesoftware.**
 -keep class org.jbundle.util.**
 -keepattributes SourceFile,LineNumberTable
 -dontwarn com.onesignal.**

 -keep class com.google.android.gms.common.api.GoogleApiClient {
     void connect();
     void disconnect();
 }


 -keep public interface android.app.OnActivityPausedListener {*;}
 -keep class com.onesignal.ActivityLifecycleListenerCompat** {*;}


 -keep class com.onesignal.OSSubscriptionState {
     void changed(com.onesignal.OSPermissionState);
 }

 -keep class com.onesignal.OSPermissionChangedInternalObserver {
     void changed(com.onesignal.OSPermissionState);
 }

 -keep class com.onesignal.OSSubscriptionChangedInternalObserver {
     void changed(com.onesignal.OSSubscriptionState);
 }

 -keep class ** implements com.onesignal.OSPermissionObserver {
     void onOSPermissionChanged(com.onesignal.OSPermissionStateChanges);
 }

 -keep class ** implements com.onesignal.OSSubscriptionObserver {
     void onOSSubscriptionChanged(com.onesignal.OSSubscriptionStateChanges);
 }

 -keep class com.onesignal.shortcutbadger.impl.AdwHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.ApexHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.AsusHomeLauncher { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.DefaultBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.EverythingMeHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.HuaweiHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.LGHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.NewHtcHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.NovaHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.OPPOHomeBader { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.SamsungHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.SonyHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.VivoHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.XiaomiHomeBadger { <init>(...); }
 -keep class com.onesignal.shortcutbadger.impl.ZukHomeBadger { <init>(...); }


 -dontwarn com.amazon.**

 -keep public class com.onesignal.ADMMessageHandler {*;}

 -keep class com.onesignal.JobIntentService$* {*;}