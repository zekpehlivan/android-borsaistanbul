package com.borsaistanbul.mobil;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.borsaistanbul.mobil.activities.BildirimDetay;
import com.borsaistanbul.mobil.activities.Splash;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class App extends Application {
    public String onSignalId = "0";
    private boolean isDataSet = false;
    String myCustomData;

    @Override
    public void onCreate ( ) {
        super.onCreate ( );
        OneSignal.startInit ( this )
                .inFocusDisplaying ( OneSignal.OSInFocusDisplayOption.Notification )
                .unsubscribeWhenNotificationsAreDisabled ( true )
                .setNotificationOpenedHandler ( new MyNotificationOpenedHandler ( this ) ).init ( );
        OneSignal.idsAvailable ( new OneSignal.IdsAvailableHandler ( ) {
            @Override
            public void idsAvailable ( String userId, String registrationId ) {
                if ( userId == null ) {
                    onSignalId = "0";
                } else {
                    onSignalId = userId;
                }
            }
        } );
    }

    public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        private Application application;

        public MyNotificationOpenedHandler ( Application application ) {
            this.application = application;
        }

        @Override
        public void notificationOpened ( OSNotificationOpenResult result ) {

            JSONObject data = result.notification.payload.additionalData;
            if ( data != null ) {
                myCustomData = data.optString ( "bildirimID", null );
                isDataSet = true;
            }

            OSNotificationAction.ActionType actionType = result.action.type;
            if ( actionType == OSNotificationAction.ActionType.ActionTaken )
                Log.i ( "OneSignalExample", "Button pressed with id: " + result.action.actionID );

            startApp ( );
        }

        private void startApp ( ) {
            if ( isDataSet ) {
                Intent intent = new Intent ( application, BildirimDetay.class )
                        .setFlags ( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK )
                        .putExtra ( "bid", myCustomData )
                        .putExtra ( "ref", "1" );
                application.startActivity ( intent );

            } else {
                Intent intent = new Intent ( application, Splash.class )
                        .setFlags ( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK );
                application.startActivity ( intent );
            }

        }
    }
}
