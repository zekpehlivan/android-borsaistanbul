package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.helper.LoaderDialog;

public class DashBoard extends Fragment {

    WebView dashView;
    public DashBoard() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity) this.getActivity()).changeIcon("dashboard");

        View row = inflater.inflate(R.layout.fragment_dash_board, container, false);
        dashView = row.findViewById(R.id.dashView);
        dashView.getSettings().setJavaScriptEnabled(true);
        dashView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        dashView.setWebChromeClient(new WebChromeClient());
        dashView.loadUrl("http://service.elluga.net/borsadashboard/");
        final LoaderDialog dialog = new LoaderDialog(getActivity());
        dialog.show();
        dashView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("BIST")) {
                    openIntent(url);
                    return true;
                } else {
                    return false;
                }

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                dialog.dismiss();
            }
        });

        return row;
    }

    private void openIntent(String url) {
        String uri = url.replace("https://service.elluga.net/borsadashboard/", "").replace("http://service.elluga.net/borsadashboard/", "");
        Intent c = new Intent(getActivity(), HisseDetay.class);
        if (uri.contains("E.BIST")) {
            c.putExtra("hisse", "e");
        }
        c.putExtra("c", uri);
        startActivity(c);
    }


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
