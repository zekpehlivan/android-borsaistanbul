package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.adapters.EndekslerAdapter;
import com.borsaistanbul.mobil.adapters.EndekslerItem;
import com.borsaistanbul.mobil.adapters.TahvilAdapter;
import com.borsaistanbul.mobil.adapters.TahvilItems;
import com.borsaistanbul.mobil.adapters.VarantAdapter;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.google.common.collect.ComparisonChain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class Tahviller extends Fragment implements TextWatcher {
    ListView listItem;
    TahvilAdapter adapter;
    ArrayList<TahvilItems> filteredList;
    ArrayList<TahvilItems> mainList;
    boolean needLoader = false;
    private Runnable runnable;
    private Timer timer;
    private Handler handler;
    EditText query;

    public Tahviller() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_tahviller, container, false);
        listItem = row.findViewById(R.id.listItem);
        listItem.setScrollingCacheEnabled(false);
        mainList = new ArrayList<>();
        filteredList = new ArrayList<>();
        adapter = new TahvilAdapter(getContext(), R.layout.varant_list_item, filteredList);
        listItem.setAdapter(adapter);
        query = row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        handler = new Handler();
        new getTahvilList().execute("");



        return row;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        callFilter();
    }


    @SuppressLint("StaticFieldLeak")
    private class getTahvilList extends AsyncTask<Object, Integer, String> {
        LoaderDialog dialog;
        String result;

        @Override
        protected void onPreExecute() {
            if (getActivity() != null) {
                dialog = new LoaderDialog(getActivity());
                if (!needLoader) {
                    dialog.show();
                }
            }

        }

        @Override
        protected String doInBackground(Object... objects) {
            HttpClient client = new HttpClient();
            try {
                if (isAdded()) {
                    result = client.Get(getResources().getString(R.string.forexTahvilApi) + getResources().getString(R.string.forex_fields));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (result != null) {
                if (mainList.size() > 0) {
                    mainList.clear();
                }
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String bilesikEndeks = (object.has("compoundRate")) ? object.getString("compoundRate") : "0";
                        String basitEndeks = (object.has("simpleRate")) ? object.getString("simpleRate") : "0";
                        String time = (object.has("time")) ? object.getString("time") : "0";
                        String legacyCode = (object.has("legacyCode")) ? object.getString("legacyCode") : "0";
                        String dateDif = (object.has("diffDays")) ? object.getString("diffDays") : "0";
                        //String volume = object.getString("dailyVolume");
                        int count = Integer.parseInt(dateDif);
                        if (count == 0) {
                            mainList.add(new TahvilItems(basitEndeks, bilesikEndeks, legacyCode, time, dateDif, "0"));
                        }
                    }


                    callFilter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if ((dialog != null) && dialog.isShowing()) {
                dialog.dismiss();
                needLoader = true;
            }


        }
    }

    private void callFilter() {
        String text = clearTurkishChar(query.getText().toString().toLowerCase().trim());

        if (text.length() > 0) {
            cleearFilters();

            for (int i = 0; i < mainList.size(); i++) {
                TahvilItems item = mainList.get(i);

                if (mainList.get(i).getLegacyCode().toLowerCase().contains(text)) {
                    filteredList.add(item);
                }
            }

            adapter.notifyDataSetChanged();

        } else {
            cleearFilters();

            filteredList.addAll(mainList);


            adapter.notifyDataSetChanged();


        }
        Collections.sort(filteredList, new Comparator<TahvilItems>() {
            @Override
            public int compare(TahvilItems tahvilItems, TahvilItems t1) {
                double s = Double.parseDouble(tahvilItems.getBasitEndeks());
                double s1 = Double.parseDouble(t1.getBasitEndeks());
                return ComparisonChain.start().compare(s1, s).result();
               // return -1;
            }
        });
    }

    public void cleearFilters() {
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
    }


    public String clearTurkishChar(String s) {
        return s.replace("ç", "c")
                .replace("Ç", "c")
                .replace("ğ", "g")
                .replace("Ğ", "g")
                .replace("ö", "o")
                .replace("Ö", "o")
                .replace("ı", "i")
                .replace("İ", "i")
                .replace("ş", "s")
                .replace("Ş", "s")
                .replace("ü", "u")
                .replace("Ü", "u");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        needLoader = false;
    }
}
