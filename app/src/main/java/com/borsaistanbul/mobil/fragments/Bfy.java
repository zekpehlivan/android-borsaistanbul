package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.adapters.EndekslerItem;
import com.borsaistanbul.mobil.adapters.VarantAdapter;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class Bfy extends Fragment implements AdapterView.OnItemClickListener, TextWatcher {
    ListView listItem;
    VarantAdapter adapter;
    ArrayList<EndekslerItem> filteredList;
    ArrayList<EndekslerItem> mainList;
    boolean needLoader = false;
    private Runnable runnable;
    private Timer timer;
    private Handler handler;
    EditText query;

    public Bfy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_sertifikalar, container, false);
        listItem = row.findViewById(R.id.listItem);
        listItem.setOnItemClickListener(this);
        listItem.setScrollingCacheEnabled(false);
        mainList = new ArrayList<>();
        filteredList = new ArrayList<>();
        adapter = new VarantAdapter(getContext(), R.layout.varant_list_item, filteredList);
        listItem.setAdapter(adapter);
        query = row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        handler = new Handler();

        timer = new Timer();
        runnable = new Runnable() {
            @Override
            public void run() {
                new getSertifikaList().execute("");
            }
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 100, 60000);
        return row;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        callFilter();
    }


    @SuppressLint("StaticFieldLeak")
    private class getSertifikaList extends AsyncTask<Object, Integer, String> {
        LoaderDialog dialog;
        String result;

        @Override
        protected void onPreExecute() {
            if (getActivity() != null) {
                dialog = new LoaderDialog(getActivity());
                if (!needLoader) {
                    dialog.show();
                }
            }

        }

        @Override
        protected String doInBackground(Object... objects) {
            HttpClient client = new HttpClient();
            try {
                if (isAdded()) {
                    result = client.Get(getResources().getString(R.string.searchApi) + getResources().getString(R.string.forex_fields) + "&security=F");

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (result != null) {
                if (mainList.size() > 0) {
                    mainList.clear();
                }
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String degisim = formatText(object.getString("dailyChangePercentage"), 2);
                        String gunlukDegisim = formatText(object.getString("dailyChange"), 2);
                        String sembol = object.getString("legacyCode");
                        String desc = object.getString("description");
                        String saat = object.getString("time");
                        String son = formatText(object.getString("last"), 2);
                        String name = object.getString("description");
                        String c = object.getString("c");
                        String yon = object.getString("dailyChangePercentageDirection");
                        long volume = object.getLong("dailyVolume");
                        mainList.add(new EndekslerItem(degisim, gunlukDegisim, c, son, name, saat, sembol, yon, null, null, volume, 0));
                    }


                    callFilter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if ((dialog != null) && dialog.isShowing()) {
                dialog.dismiss();
                needLoader = true;
            }


        }
    }

    private void callFilter() {
        String text = clearTurkishChar(query.getText().toString().toLowerCase().trim());

        if (text.length() > 0) {
            cleearFilters();

            for (int i = 0; i < mainList.size(); i++) {
                EndekslerItem item = mainList.get(i);

                if (mainList.get(i).getLegacyCode().toLowerCase().contains(text)) {
                    filteredList.add(item);
                } else {
                    String matches = clearTurkishChar(item.getDesc().toLowerCase());
                    if (matches.contains(text)) {
                        filteredList.add(item);
                    }
                }
            }
            sortFilter();
            adapter.notifyDataSetChanged();

        } else {
            cleearFilters();

            filteredList.addAll(mainList);

            sortFilter();
            adapter.notifyDataSetChanged();


        }

    }

    public void sortFilter() {
        Collections.sort(filteredList, new Comparator<EndekslerItem>() {
            @Override
            public int compare(EndekslerItem endekslerItem, EndekslerItem t1) {

                return Long.compare(t1.getDailyVolume(), endekslerItem.getDailyVolume());
            }
        });
    }

    public void cleearFilters() {
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
    }

    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        EndekslerItem items = (EndekslerItem) adapterView.getItemAtPosition(i);
        //Toast.makeText(getContext(), items.getCode(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext(), HisseDetay.class).putExtra("c", items.getCode()));
    }

    public String clearTurkishChar(String s) {
        return s.replace("ç", "c")
                .replace("Ç", "c")
                .replace("ğ", "g")
                .replace("Ğ", "g")
                .replace("ö", "o")
                .replace("Ö", "o")
                .replace("ı", "i")
                .replace("İ", "i")
                .replace("ş", "s")
                .replace("Ş", "s")
                .replace("ü", "u")
                .replace("Ü", "u");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        timer.cancel();
        needLoader = false;
    }
}
