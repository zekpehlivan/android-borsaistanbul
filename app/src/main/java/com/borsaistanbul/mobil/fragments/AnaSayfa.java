package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.borsaistanbul.mobil.App;
import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.activities.HisseEkle;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.adapters.AnasayfaAdapter;
import com.borsaistanbul.mobil.adapters.AnasayfaItems;
import com.borsaistanbul.mobil.adapters.HisselerAdapter;
import com.borsaistanbul.mobil.adapters.HisselerItems;
import com.borsaistanbul.mobil.helper.BounceListView;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyEditText;
import com.borsaistanbul.mobil.helper.apiInit;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import com.google.common.collect.ComparisonChain;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;


public class AnaSayfa extends Fragment implements View.OnClickListener, TextWatcher, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    FloatingActionButton fab;
    View Row;
    ArrayList<AnasayfaItems> list = new ArrayList<>();
    ArrayList<AnasayfaItems> filteredList = new ArrayList<>();
    AnasayfaAdapter adapter;
    DbHelper helper;
    BounceListView listItem;
    int itemCount;
    MyEditText query;
    boolean needLoader = false;
    Handler handler = new Handler();
    Timer timer = new Timer();
    Runnable runnable;
    String result;
    String onSignalId;
    LinearLayout welcome;
    LinearLayout content;
    ImageView btnHisseEkle;
    private FirebaseAnalytics analytics;
    SharedPreferences pref;
    StringBuilder takipEttiklerim;
    LoaderDialog dialog;
    String spnArray[] = {"İsme göre sırala", "Değişime (Artan) göre sırala", "Değişime (Azalan) göre sırala", "Hacme Göre"};
    int siralama = 0;
    Spinner spnSirala;
    Button btnSirala;

    public AnaSayfa() {

    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) this.getActivity()).changeIcon("sayfam");
        Row = inflater.inflate(R.layout.fragment_ana_sayfa, container, false);
        fab = Row.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        content = Row.findViewById(R.id.content);
        welcome = Row.findViewById(R.id.welcome);
        helper = new DbHelper(getContext());
        listItem = Row.findViewById(R.id.listItem);
        listItem.setOnItemClickListener(this);
        spnSirala = Row.findViewById(R.id.siralama);
        btnSirala = Row.findViewById(R.id.btnSirala);
        List<String> spnItems = Arrays.asList(spnArray);

        ArrayAdapter<String> spnAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, spnItems) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                return super.getView(position, convertView, parent);
            }
        };
        spnAdapter.setDropDownViewResource(R.layout.custom_spinner);
        spnSirala.setAdapter(spnAdapter);

        pref = getActivity().getSharedPreferences("borsa", Context.MODE_PRIVATE);
        query = Row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        adapter = new AnasayfaAdapter(getActivity(), R.layout.my_page_list_item, filteredList);
        listItem.setAdapter(adapter);
        listItem.enableLoadMore(false);
        createTakipList();
        runnable = new Runnable() {
            @Override
            public void run() {
                itemCount = helper.getRowCount();
                if (itemCount > 0) {
                    for (int i = 0; i < adapter.getCount(); i++) {
                        //Toast.makeText(getContext(), "test", Toast.LENGTH_SHORT).show();
                        AnasayfaItems items = (AnasayfaItems) listItem.getItemAtPosition(i);
                        if (items != null) {
                            pref.edit().putString(items.getC(), items.getSaat()).apply();
                        }

                    }
                    getHisse();
                }
            }
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 100, 5000);

        onSignalId = ((App) this.getActivity().getApplicationContext()).onSignalId;
        checkScreen();
        btnHisseEkle = Row.findViewById(R.id.btnHisseEkle);
        btnHisseEkle.setOnClickListener(this);
        btnSirala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spnSirala.performClick();
            }
        });
        sendTakipList();

        spnSirala.setOnItemSelectedListener(this);
        spnSirala.setSelection(0);
        return Row;


    }

    private void createTakipList() {
        takipEttiklerim = new StringBuilder();
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery("Select kod from list", null);
        while (c.moveToNext()) {
            takipEttiklerim.append("&c=").append(c.getString(c.getColumnIndex("kod")));
        }
        c.close();
        db.close();
    }

    private void getHisse() {
        if (getActivity() != null) {
            createTakipList();
            AsyncHttpClient client = new AsyncHttpClient();
            helper = new DbHelper(getActivity());
            apiInit api = new apiInit(getActivity());
            client.setBasicAuth(api.getApiUser(), api.getApiPass());
            String url = getResources().getString(R.string.sayfamApi) + takipEttiklerim;
            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onStart() {
                    if (getActivity() != null) {
                        dialog = new LoaderDialog(getContext());
                        if (!needLoader) {
                            dialog.show();
                        }

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String str = null;

                    str = new String(responseBody);

                    if (str.length() > 0) {
                        if (list.size() > 0) {
                            list.clear();
                        }

                        try {

                            JSONArray jsonArray = new JSONArray(str);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String degisimOran = formatText(object.getString("dailyChangePercentage"), 2);
                                String degisim = formatText(object.getString("dailyChange"), 2);
                                String sembol = object.getString("legacyCode");
                                String saat = object.getString("time");
                                String son = object.getString("last") != null ? String.format(Locale.getDefault(), "%,.2f", object.getDouble("last")) : object.getString("last");
                                String name = object.getString("description");
                                String c = object.getString("c");
                                String yon = object.getString("dailyChangePercentageDirection");
                                Long hacim = object.getLong("dailyVolume");
                                list.add(new AnasayfaItems(sembol, degisimOran, son, saat, name, c, yon, degisim, hacim));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    searchFilter();

                    sortList(siralama);

                    if (getActivity() != null) {

                        dialog.dismiss();
                        needLoader = true;

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                }
            });
        }

    }

    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }


    private void searchFilter() {
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
        String searchText = clearTurkishChar(query.getText().toString().toUpperCase(new Locale("tr-TR")).trim());
        if (searchText.length() > 0) {
            for (int i = 0; i < list.size(); i++) {
                AnasayfaItems item = (AnasayfaItems) list.get(i);
                if (item.getSembol().toUpperCase(new Locale("tr-TR")).contains(searchText)) {
                    filteredList.add(item);
                } else {
                    if (item.getName().contains(searchText))
                        filteredList.add(item);
                }
            }
            adapter.notifyDataSetChanged();
        } else {
            filteredList.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }

    private void checkScreen() {
        itemCount = helper.getRowCount();

        if (itemCount > 0) {
            welcome.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else {
            welcome.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
    }

    public String clearTurkishChar(String s) {
        return s.replace("ç", "c")
                .replace("Ç", "c")
                .replace("ğ", "g")
                .replace("Ğ", "g")
                .replace("ö", "o")
                .replace("Ö", "o")
                .replace("ı", "i")
                .replace("İ", "i")
                .replace("ş", "s")
                .replace("Ş", "s")
                .replace("ü", "u")
                .replace("Ü", "u");

    }

    private void sendTakipList() {
        @SuppressLint("HardwareIds") String phoneID = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String signalID = onSignalId;
        StringBuilder takipList = new StringBuilder("_");
        DbHelper helper = new DbHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery("Select * from list", null, null);
        while (c.moveToNext()) {
            String item = c.getString(c.getColumnIndex("kod"));
            if (item.contains("E.BIST"))
                takipList.append(item.replace(".E.BIST", "")).append("_");

        }
        c.close();
        db.close();
      /*  for(int i=0;i<list.size();i++){
            AnasayfaItems item = list.get(i);
            if(item.getC().contains("E.BIST"))
            takipList.append(item.getSembol()).append("_");

        }
*/
        if (!takipList.toString().equals("_")) {
            takipList = new StringBuilder(takipList.substring(1, takipList.length() - 1));

        }

         sendList(phoneID, signalID, takipList.toString());
    }


    @Override
    public void onResume() {
        super.onResume();
        if (adapter.getCount() > 0) {
            timer.cancel();
            adapter.notifyDataSetChanged();
            getHisse();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(runnable);
                }
            }, 100, 5000);
        }
        if (!query.getText().toString().equals("")) {
            query.setText("");
        }
        sendTakipList();
        checkScreen();
    }

    @Override
    public void onClick(View view) {
        if (view == fab) {
            startActivity(new Intent(getContext(), HisseEkle.class));
        }
        if (view == btnHisseEkle) {
            fab.performClick();
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        searchFilter();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        boolean hisse = false;

        AnasayfaItems anasayfaItems = (AnasayfaItems) adapterView.getItemAtPosition(i);
        hisse = anasayfaItems.getC().contains("E.BIST");
        Intent c = new Intent(getActivity(), HisseDetay.class);
        if (hisse) {
            c.putExtra("hisse", "e");
        }
        c.putExtra("c", anasayfaItems.getC());
        startActivity(c);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //   Toast.makeText(getActivity(), ""+i, Toast.LENGTH_SHORT).show();
        siralama = i;
        sortList(i);
    }

    private void sortList(int i) {
        switch (i) {
            case 0:
                Collections.sort(filteredList, new Comparator<AnasayfaItems>() {
                    @Override
                    public int compare(AnasayfaItems anasayfaItems, AnasayfaItems t1) {
                        return ComparisonChain.start().compare(anasayfaItems.getSembol(), t1.getSembol()).result();
                    }
                });
                adapter.notifyDataSetChanged();
                break;
            case 1:
                Collections.sort(filteredList, new Comparator<AnasayfaItems>() {
                    @Override
                    public int compare(AnasayfaItems anasayfaItems, AnasayfaItems t1) {
                        NumberFormat format = NumberFormat.getInstance(new Locale("tr","TR"));

                        double s = 0;
                        double s1 =0;
                        try {
                            s = (double) format.parse (anasayfaItems.getGunlukDegisim() );
                            s1 = (double) format.parse ( t1.getGunlukDegisim()  );

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        return ComparisonChain.start().compare(s1, s).result();
                    }
                });
                adapter.notifyDataSetChanged();

                break;
            case 2:
                Collections.sort(filteredList, new Comparator<AnasayfaItems>() {
                    @Override
                    public int compare(AnasayfaItems anasayfaItems, AnasayfaItems t1) {
                        NumberFormat format = NumberFormat.getInstance(new Locale("tr","TR"));

                        double s = 0;
                        double s1 =0;
                        try {
                            s = (double) format.parse (anasayfaItems.getGunlukDegisim() );
                            s1 = (double) format.parse ( t1.getGunlukDegisim()  );

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        return ComparisonChain.start().compare(s, s1).result();
                    }
                });
                adapter.notifyDataSetChanged();

                break;
            case 3:
                Collections.sort(filteredList, new Comparator<AnasayfaItems>() {
                    @Override
                    public int compare(AnasayfaItems anasayfaItems, AnasayfaItems t1) {
                        long s = anasayfaItems.getHacim();
                        long s1 = t1.getHacim();
                        return Long.compare(s1, s);
                    }
                });
                adapter.notifyDataSetChanged();

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public void  sendList(String phoneID, String signalID, String takipList){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.setConnectTimeout(20*10000);
        client.setTimeout(20*10000);
        client.setResponseTimeout(20*10000);
        client.get("http://service.elluga.net/api/servis/kap/talep/takip/phid/" + phoneID + "/sigid/" + signalID + "/follow/" + takipList + "/app/" + "Android", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                System.out.println(new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });
    }
}

