package com.borsaistanbul.mobil.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.adapters.FmPagerAdapter;
import com.borsaistanbul.mobil.helper.CustomViewPager;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.borsaistanbul.mobil.helper.SlidingTabLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class Piyasalar extends Fragment {
    SlidingTabLayout sliderLayout;
    CustomViewPager tabPager;
    FmPagerAdapter adapter;
    MyTextView date;


    public Piyasalar() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (adapter == null) {
            this.adapter = new FmPagerAdapter(getChildFragmentManager(), getActivity());

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity) this.getActivity()).changeIcon("piyasalar");

        return inflater.inflate(R.layout.fragment_piyasalar, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        date = view.findViewById(R.id.date);
        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM EEEE", Locale.getDefault());
        String dateOfDay = df.format(d);
        date.setText(dateOfDay);
        sliderLayout = view.findViewById(R.id.tabSlider);
        tabPager = view.findViewById(R.id.tabPager);
        tabPager.setOffscreenPageLimit(0);

        tabPager.setAdapter(adapter);

        sliderLayout.setDistributeEvenly(true);
        sliderLayout.setSelectedIndicatorColors(getResources().getColor(R.color.navColor));
        sliderLayout.setViewPager(tabPager);
        sliderLayout.initFirstTabColor();
    }
}
