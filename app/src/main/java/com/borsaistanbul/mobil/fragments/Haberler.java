package com.borsaistanbul.mobil.fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HaberDetay;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.adapters.HaberAdapter;
import com.borsaistanbul.mobil.adapters.HaberItems;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Haberler extends Fragment {
    ListView itemList;
    View row;
    private FirebaseAnalytics analytics;

    public Haberler() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) this.getActivity()).changeIcon("duyuru");
        row = inflater.inflate(R.layout.fragment_haberler, container, false);
        itemList = row.findViewById(R.id.itemList);
        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HaberItems item = (HaberItems) adapterView.getItemAtPosition(i);
                startActivity ( new Intent ( getContext ( ), HaberDetay.class ).putExtra ( "img", item.getImg ( ) ).putExtra ( "text", item.getText ( ) ).putExtra ( "title", item.getTitle ( ) ) );
            }
        });
        new gethabers().execute("");

        return row;
    }

    @SuppressLint("StaticFieldLeak")
    private class gethabers extends AsyncTask<Object, Object, String> {
        LoaderDialog dialog;
        String result;

        @Override
        protected void onPreExecute() {
            dialog = new LoaderDialog ( getContext ( ) );
            dialog.show();
        }

        @Override
        protected String doInBackground(Object... objects) {
            HttpClient client = new HttpClient();
            try {
                result = client.Get("https://bist.elluga.net/service.php?secure=MY_SECURITY_KEY&type=duyuru");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            ArrayList<HaberItems> items = new ArrayList<>();
            if (result != null) {
                try {
                    JSONArray jarr = new JSONArray(result);
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject jObj = jarr.getJSONObject(i);
                        String d_id = jObj.getString("duyuru_id");
                        String d_img = jObj.getString("duyuru_img");
                        String d_text = jObj.getString("duyuru_text");
                        String d_title = jObj.getString("duyuru_title");
                        items.add(new HaberItems(d_id, d_img, d_text, d_title));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (getActivity() != null) {
                    HaberAdapter adapter = new HaberAdapter ( getContext ( ), R.layout.haber_items, items );
                    itemList.setAdapter(adapter);
                }

            }
            dialog.dismiss();
        }
    }
}
