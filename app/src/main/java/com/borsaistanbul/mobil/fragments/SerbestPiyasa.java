package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.adapters.EndekslerItem;
import com.borsaistanbul.mobil.adapters.VarantAdapter;
import com.borsaistanbul.mobil.adapters.sPiyasaAdapter;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SerbestPiyasa extends Fragment implements AdapterView.OnItemClickListener, TextWatcher {
    ListView listItem;
    sPiyasaAdapter adapter;
    ArrayList<EndekslerItem> filteredList;
    ArrayList<EndekslerItem> mainList;
    boolean needLoader = false;
    private Runnable runnable;
    private Timer timer;
    private Handler handler;
    EditText query;

    public SerbestPiyasa() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_serbest_piyasa, container, false);
        listItem = row.findViewById(R.id.listItem);
        listItem.setOnItemClickListener(this);
        mainList = new ArrayList<>();
        filteredList = new ArrayList<>();
        adapter = new sPiyasaAdapter(getContext(), R.layout.piyasa_list_item, filteredList);
        listItem.setAdapter(adapter);
        listItem.setScrollingCacheEnabled(false);
        query = row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        handler = new Handler();

        timer = new Timer();
        runnable = new Runnable() {
            @Override
            public void run() {
                new getSertifikaList().execute("");
            }
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 100, 2000);
        return row;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        callFilter();
    }


    @SuppressLint("StaticFieldLeak")
    private class getSertifikaList extends AsyncTask<Object, Integer, String> {
        LoaderDialog dialog;
        String result;

        @Override
        protected void onPreExecute() {
            if (getActivity() != null) {
                dialog = new LoaderDialog(getActivity());
                if (!needLoader) {
                    dialog.show();
                }
            }

        }

        @Override
        protected String doInBackground(Object... objects) {
            HttpClient client = new HttpClient();
            try {
                if (isAdded()) {
                    result = client.Get(getResources().getString(R.string.forexPiyasaApi) + getResources().getString(R.string.forex_fields));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (result != null) {
                if (mainList.size() > 0) {
                    mainList.clear();
                }
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String degisim = formatText(object.getString("dailyChangePercentage"), 2);
                        String gunlukDegisim = formatText(object.getString("dailyChange"), 2);
                        String sembol = object.getString("legacyCode");
                        String desc = object.getString("description");
                        String saat = object.getString("time");
                        String son = formatText(object.getString("last"), 2);
                        String name = object.getString("description");
                        String c = object.getString("c");
                        String yon = object.getString("dailyChangePercentageDirection");

                        String ask = (object.has("ask")) ? formatText(object.getString("ask"), ondalikDuzelt(sembol)) : null;
                        String bid = (object.has("bid")) ? formatText(object.getString("bid"), ondalikDuzelt(sembol)) : null;
                        mainList.add(new EndekslerItem(degisim, gunlukDegisim, c, son, name, saat, sembol, yon, ask, bid, (long) 0, 0));
                    }

                    changePositon(0, findPosition("SUSD"));
                    changePositon(1, findPosition("SEUR"));
                    changePositon(2, findPosition("SSAR"));
                    changePositon(3, findPosition("SCAD"));
                    changePositon(4, findPosition("SCHF"));
                    changePositon(5, findPosition("SGBP"));
                    changePositon(6, findPosition("SJPY"));
                    changePositon(7, findPosition("SGLD"));
                    changePositon(8, findPosition("SGCEYREK"));
                    changePositon(9, findPosition("SGYARIM"));
                    changePositon(10, findPosition("SCUM"));
                    changePositon(11, findPosition("SGATA"));
                    changePositon(12, findPosition("SGBESLI"));
                    changePositon(13, findPosition("SGIKIBUCUK"));
                    changePositon(14, findPosition("SGGREMSE"));
                    changePositon(15, findPosition("SG14BIL"));
                    changePositon(16, findPosition("SG18BIL"));
                    changePositon(17, findPosition("SG22BIL"));
                    changePositon(18, findPosition("USGLD"));
                    changePositon(19, findPosition("EURGLD"));
                    changePositon(20, findPosition("XGLD"));
                    changePositon(21, findPosition("XHGLD"));
                    callFilter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if ((dialog != null) && dialog.isShowing()) {
                dialog.dismiss();
                needLoader = true;
            }


        }
    }

    public int ondalikDuzelt(String sembol) {
        int ondalik = 0;
        switch (sembol) {
            case "SUSD":
                ondalik = 4;
                break;
            case "SEUR":
                ondalik = 4;
                break;
            case "SSAR":
                ondalik = 4;
                break;
            case "SCAD":
                ondalik = 4;
                break;
            case "SCHF":
                ondalik = 4;
                break;
            case "SGBP":
                ondalik = 4;
                break;
            case "SJPY":
                ondalik = 4;
                break;
            case "XGLD":
                ondalik = 3;
                break;
            case "SGCEYREK":
                ondalik = 2;
                break;
            case "SGYARIM":
                ondalik = 2;
                break;
            case "SCUM":
                ondalik = 0;
                break;
            case "SGATA":
                ondalik = 2;
                break;
            case "SGBESLI":
                ondalik = 2;
                break;
            case "SGIKIBUCUK":
                ondalik = 2;
                break;
            case "SGGREMSE":
                ondalik = 2;
                break;
            case "SG14BIL":
                ondalik = 2;
                break;
            case "SG18BIL":
                ondalik = 2;
                break;
            case "SG22BIL":
                ondalik = 2;
                break;
            case "SGZIYNET":
                ondalik = 2;
                break;
            case "EURGLD":
                ondalik = 0;
                break;
            case "USGLD":
                ondalik = 0;
                break;
            case "USGLDKG":
                ondalik = 0;
                break;
            case "SGLD":
                ondalik = 3;
                break;
            case "XHGLD":
                ondalik = 3;
                break;
            default:
                ondalik = 4;
                break;
        }
        return ondalik;
    }


    private void callFilter() {
        String text = clearTurkishChar(query.getText().toString().toLowerCase().trim());

        if (text.length() > 0) {
            cleearFilters();

            for (int i = 0; i < mainList.size(); i++) {
                EndekslerItem item = mainList.get(i);

                if (mainList.get(i).getLegacyCode().toLowerCase().contains(text)) {
                    filteredList.add(item);
                } else {
                    String matches = clearTurkishChar(item.getDesc().toLowerCase());
                    if (matches.contains(text)) {
                        filteredList.add(item);
                    }
                }
            }

            adapter.notifyDataSetChanged();

        } else {
            cleearFilters();
            filteredList.addAll(mainList);



            adapter.notifyDataSetChanged();


        }

    }

    public void changePositon(int pos, int pos2) {
        if (pos2 > 0) {
            // filteredList.remove(pos2);
            // filteredList.add(pos, mainList.get(pos2));
            Collections.swap(mainList, pos2, pos);
            // adapter.notifyDataSetChanged();
        }


    }

    public int findPosition(String sembol) {
        int position = 0;
        for (int j = 0; j < mainList.size(); j++) {
            EndekslerItem item = mainList.get(j);
            if (item.getLegacyCode().equalsIgnoreCase(sembol)) {
                position = j;
            }
        }
        return position;
    }

    public void cleearFilters() {
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
    }

    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        EndekslerItem items = (EndekslerItem) adapterView.getItemAtPosition(i);
        //Toast.makeText(getContext(), items.getCode(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext(), HisseDetay.class).putExtra("c", items.getCode()));
    }

    public String clearTurkishChar(String s) {
        return s.replace("ç", "c")
                .replace("Ç", "c")
                .replace("ğ", "g")
                .replace("Ğ", "g")
                .replace("ö", "o")
                .replace("Ö", "o")
                .replace("ı", "i")
                .replace("İ", "i")
                .replace("ş", "s")
                .replace("Ş", "s")
                .replace("ü", "u")
                .replace("Ü", "u");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        timer.cancel();
        needLoader = false;
    }
}
