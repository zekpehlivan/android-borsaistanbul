package com.borsaistanbul.mobil.fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.BildirimDetay;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.adapters.BildirimAdapter;
import com.borsaistanbul.mobil.adapters.BildirimItems;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyEditText;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class Kap extends Fragment implements TextWatcher, View.OnClickListener {
    View row;
    Button tumSirket;
    Button takipEttiklerim;
    ListView listItem;
    MyEditText query;
    MyTextView date;
    BildirimAdapter adapter;
    BildirimAdapter adapter1;
    BildirimAdapter adapter2;
    ArrayList<BildirimItems> list = new ArrayList<>();
    ArrayList<BildirimItems> filteredList = new ArrayList<>();
    ArrayList<BildirimItems> myList = new ArrayList<>();
    DbHelper helper;
    int type = 0;
    private FirebaseAnalytics analytics;

    public Kap() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) this.getActivity()).changeIcon("kap");

        row = inflater.inflate(R.layout.fragment_kap, container, false);
        tumSirket = row.findViewById(R.id.tum_sirketler);
        tumSirket.setOnClickListener(this);
        takipEttiklerim = row.findViewById(R.id.takip_ettiklerim);
        takipEttiklerim.setOnClickListener(this);
        listItem = row.findViewById(R.id.listItem);
        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BildirimItems items = (BildirimItems) adapterView.getItemAtPosition(i);
                startActivity(new Intent(getContext(), BildirimDetay.class)
                        .putExtra("unvan", items.getUnvan())
                        .putExtra("bid", items.getBildirimNo()));
            }
        });
        query = row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        date = row.findViewById(R.id.date);
        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        String dateOfDay = df.format(d);
        date.setText(dateOfDay);
        getBildirimler();
        adapter = new BildirimAdapter(getContext(), R.layout.kap_fragment_list_item, list);
        adapter1 = new BildirimAdapter(getContext(), R.layout.kap_fragment_list_item, filteredList);
        adapter2 = new BildirimAdapter(getContext(), R.layout.kap_fragment_list_item, myList);
        tumSirket.setSelected(true);
        helper = new DbHelper(getContext());

        return row;
    }

    private void getBildirimler() {
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.setConnectTimeout(20*10000);
        client.setTimeout(20*10000);
        client.setResponseTimeout(20*10000);
        client.get("http://service.elluga.net/api/servis/kap/talep/gunlukbildirim/format/json", new AsyncHttpResponseHandler() {
            LoaderDialog dialog;

            @Override
            public void onStart() {
                if(getActivity()!=null){
                    dialog = new LoaderDialog(getContext());
                    dialog.show();
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody != null) {
                    String str = new String(responseBody);
                    try {
                        JSONArray arr = new JSONArray(str);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            String kod = obj.getString("sirket_kodu");
                            String unvan = obj.getString("sirket_adi");
                            String title = obj.getString("bildirim_baslik");
                            String date = obj.getString("yayinlanma_tarihi_formatli");
                            String bildirimNo = obj.getString("bildirim_numarasi");
                            SimpleDateFormat df = new SimpleDateFormat("dd.mm.yy HH:MM", Locale.getDefault());
                            Date d = df.parse(date);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM", Locale.getDefault());
                            String saat = dateFormat.format(d);
                            list.add(new BildirimItems(kod, saat, unvan, title, bildirimNo));
                        }
                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                    }

                    listItem.setAdapter(adapter);


                }
                if(getActivity()!=null) {
                    dialog.dismiss();
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        filter();


    }

    private void filter() {
        String text = query.getText().toString().toUpperCase().trim()
                .replace("İ", "I")
                .replace("Ş", "S")
                .replace("Ü", "U")
                .replace("Ö", "O")
                .replace("Ç", "C")
                .replace("Ğ", "G");
        String txtUnvan = query.getText().toString().toUpperCase().trim();
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
        if (type == 0) {
            if (text.length() > 0) {
                //  filteredList.clear();
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getKod().contains(text)) {

                        filteredList.add(list.get(j));
                    } else {
                        String item = list.get(j).getUnvan()
                                .replace("İ", "I")
                                .replace("Ş", "S")
                                .replace("Ü", "U")
                                .replace("Ö", "O")
                                .replace("Ç", "C")
                                .replace("Ğ", "G");

                        if (item.contains(text)) {
                            filteredList.add(list.get(j));
                        }
                    }
                }
                listItem.setAdapter(adapter1);

            } else {

                listItem.setAdapter(adapter);

            }
        } else {
            if (text.length() > 0) {
                //  filteredList.clear();
                for (int j = 0; j < myList.size(); j++) {
                    if (myList.get(j).getKod().contains(text)) {

                        filteredList.add(myList.get(j));
                    } else {
                        String item = myList.get(j).getUnvan()
                                .replace("İ", "I")
                                .replace("Ş", "S")
                                .replace("Ü", "U")
                                .replace("Ö", "O")
                                .replace("Ç", "C")
                                .replace("Ğ", "G");

                        if (item.contains(text)) {
                            filteredList.add(myList.get(j));
                        }
                    }
                }
                listItem.setAdapter(adapter1);

            } else {

                listItem.setAdapter(adapter2);

            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == tumSirket) {
            tumSirket.setSelected(true);
            takipEttiklerim.setSelected(false);
            listItem.setAdapter(adapter);
            type = 0;
            filter();

        }
        if (view == takipEttiklerim) {
            tumSirket.setSelected(false);
            takipEttiklerim.setSelected(true);
            callMyList(list);
            type = 1;
            filter();
        }
    }

    private void callMyList(ArrayList<BildirimItems> liste) {
        myList.clear();
        for (int i = 0; i < liste.size(); i++) {
            boolean varmi = helper.isAdded(liste.get(i).getKod() + ".E.BIST");
            //Toast.makeText(getContext(), liste.get ( i ).getKod(), Toast.LENGTH_SHORT).show();
            if (varmi) {
                myList.add(liste.get(i));
            }

        }
        listItem.setAdapter(adapter2);
    }


}
