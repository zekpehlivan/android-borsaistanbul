package com.borsaistanbul.mobil.fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.activities.SirketDetay;
import com.borsaistanbul.mobil.adapters.SirketlerAdapter;
import com.borsaistanbul.mobil.adapters.SirketlerItems;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyEditText;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Sirketler extends Fragment implements TextWatcher, AdapterView.OnItemClickListener {
    ListView listItem;
    View row;
    MyEditText search;
    ArrayList <SirketlerItems> list = new ArrayList <> ( );
    ArrayList <SirketlerItems> filteredList = new ArrayList <> ( );
    SirketlerAdapter adapter;
    SirketlerAdapter adapter1;

    public Sirketler() {
    }


    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState ) {
        ((MainActivity) this.getActivity()).changeIcon("sirketler");
        row = inflater.inflate(R.layout.fragment_sirketler, container, false);
        listItem = row.findViewById ( R.id.listItem );
        listItem.setOnItemClickListener ( this );
        search = row.findViewById ( R.id.query );
        search.addTextChangedListener ( this );
        adapter1 = new SirketlerAdapter ( getContext ( ), R.layout.sirketler_items, filteredList );
        adapter = new SirketlerAdapter ( getContext ( ), R.layout.sirketler_items, list );
        sirketGetir();

        return row;
    }

    private void sirketGetir() {
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.setConnectTimeout(20*10000);
        client.setTimeout(20*10000);
        client.setResponseTimeout(20*10000);
        client.get("http://service.elluga.net/api/servis/kap/talep/bistsirketliste/format/json", new AsyncHttpResponseHandler() {
            LoaderDialog dialog;

            @Override
            public void onStart() {
                if(getActivity()!=null){
                    dialog = new LoaderDialog ( getContext ( ) );
                    dialog.show ( );
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(responseBody!=null){
                    try {
                        JSONArray arr = new JSONArray ( new String(responseBody) );
                        for ( int i = 0 ; i < arr.length ( ) ; i++ ) {
                            JSONObject obj = arr.getJSONObject ( i );
                            String kod = obj.getString ( "sirket_kodu" );
                            String unvan = obj.getString ( "sirket_unvani" );
                            String grup = obj.getString ( "sirket_grubu" );
                            String sehir = obj.getString ( "sirket_sehri" );
                            String denetmen = obj.getString ( "denetim_kurulusu" );
                            String link = obj.getString ( "ozet_linki" );
                            list.add ( new SirketlerItems ( kod, unvan, grup, sehir, denetmen, link ) );
                        }

                        listItem.setAdapter ( adapter );

                    } catch ( JSONException e ) {
                        e.printStackTrace ( );
                    }
                    if(getActivity()!=null){
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    @Override
    public void onItemClick ( AdapterView <?> adapterView, View view, int i, long l ) {
        SirketlerItems items = (SirketlerItems) adapterView.getItemAtPosition ( i );
        startActivity ( new Intent ( getContext ( ), SirketDetay.class ).putExtra ( "c", items.getLink ( ) ).putExtra ( "u", items.getUnvan ( ) ) );
    }


    @Override
    public void beforeTextChanged ( CharSequence charSequence, int i, int i1, int i2 ) {

    }

    @Override
    public void onTextChanged ( CharSequence charSequence, int i, int i1, int i2 ) {

    }

    @Override
    public void afterTextChanged ( Editable editable ) {
        String text = search.getText ( ).toString ( ).toUpperCase ( ).trim ( )
                .replace ( "İ", "I" )
                .replace ( "Ş", "S" )
                .replace ( "Ü", "U" )
                .replace ( "Ö", "O" )
                .replace ( "Ç", "C" )
                .replace ( "Ğ", "G" );
        String txtUnvan = search.getText ( ).toString ( ).toUpperCase ( ).trim ( );
        if ( filteredList.size ( ) > 0 ) {
            filteredList.clear ( );
        }
        if ( text.length ( ) > 0 ) {
            //  filteredList.clear();
            for ( int j = 0 ; j < list.size ( ) ; j++ ) {
                if ( list.get ( j ).getKod ( ).contains ( text ) ) {

                    filteredList.add ( list.get ( j ) );
                } else {
                    String item = list.get ( j ).getUnvan ( )
                            .replace ( "İ", "I" )
                            .replace ( "Ş", "S" )
                            .replace ( "Ü", "U" )
                            .replace ( "Ö", "O" )
                            .replace ( "Ç", "C" )
                            .replace ( "Ğ", "G" );

                    if ( item.contains ( text ) ) {
                        filteredList.add ( list.get ( j ) );
                    }
                }
            }
            listItem.setAdapter ( adapter1 );

        } else {

            listItem.setAdapter ( adapter );

        }
    }

}
