package com.borsaistanbul.mobil.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.activities.Hisseler;
import com.borsaistanbul.mobil.adapters.EndekslerItem;
import com.borsaistanbul.mobil.adapters.HisseEndeksAdapter;
import com.borsaistanbul.mobil.adapters.HisseEndeksListItem;
import com.borsaistanbul.mobil.adapters.VarantAdapter;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

public class HisseEndeks extends Fragment implements AdapterView.OnItemClickListener, TextWatcher {
    ListView listItem;
    HisseEndeksAdapter adapter;
    ArrayList<HisseEndeksListItem> filteredList;
    ArrayList<HisseEndeksListItem> mainList;
    boolean needLoader = false;
    private Runnable runnable;
    private Timer timer;
    private Handler handler;
    EditText query;

    public HisseEndeks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_hisse_endeks, container, false);
        listItem = row.findViewById(R.id.listItem);
        listItem.setOnItemClickListener(this);
        listItem.setScrollingCacheEnabled(false);
        mainList = new ArrayList<>();
        filteredList = new ArrayList<>();
        adapter = new HisseEndeksAdapter(getContext(), R.layout.hisse_endeks_list_item, filteredList);
        listItem.setAdapter(adapter);
        query = row.findViewById(R.id.query);
        query.addTextChangedListener(this);
        handler = new Handler();

        endeksliste();
        return row;
    }

    private void endeksliste() {
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.setConnectTimeout(20*10000);
        client.setTimeout(20*10000);
        client.setResponseTimeout(20*10000);
        client.get(getResources().getString(R.string.hisseEndeksApi), new AsyncHttpResponseHandler() {
            LoaderDialog dialog;
            @Override
            public void onStart() {
                if (getActivity() != null) {
                    dialog = new LoaderDialog(getActivity());
                    if (!needLoader) {
                        dialog.show();
                    }
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(responseBody!=null){
                    if (mainList.size() > 0) {
                        mainList.clear();
                    }
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);

                            String sembol = object.getString("legacyCode");
                            String desc = object.getString("description");
                            String c = object.getString("c");
                            mainList.add(new HisseEndeksListItem(c, sembol, desc));
                        }


                        callFilter();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                    needLoader = true;
                }
                }



            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        callFilter();
    }


    private void callFilter() {
        String text = clearTurkishChar(query.getText().toString().toLowerCase().trim());

        if (text.length() > 0) {
            cleearFilters();

            for (int i = 0; i < mainList.size(); i++) {
                HisseEndeksListItem item = mainList.get(i);

                if (mainList.get(i).getLegacyCode().toLowerCase().contains(text)) {
                    filteredList.add(item);
                } else {
                    String matches = clearTurkishChar(item.getDesc().toLowerCase());
                    if (matches.contains(text)) {
                        filteredList.add(item);
                    }
                }
            }

            adapter.notifyDataSetChanged();

        } else {
            cleearFilters();

            filteredList.addAll(mainList);


            adapter.notifyDataSetChanged();


        }

    }

    public void cleearFilters() {
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
    }

    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        HisseEndeksListItem items = (HisseEndeksListItem) adapterView.getItemAtPosition(i);
        //Toast.makeText(getContext(), items.getCode(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getContext(), Hisseler.class).putExtra("c", items.getLegacyCode()).putExtra("desc", items.getDesc()));
    }

    public String clearTurkishChar(String s) {
        return s.replace("ç", "c")
                .replace("Ç", "c")
                .replace("ğ", "g")
                .replace("Ğ", "g")
                .replace("ö", "o")
                .replace("Ö", "o")
                .replace("ı", "i")
                .replace("İ", "i")
                .replace("ş", "s")
                .replace("Ş", "s")
                .replace("ü", "u")
                .replace("Ü", "u");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        needLoader = false;
    }
}
