package com.borsaistanbul.mobil.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.borsaistanbul.mobil.R;


public class FontButton extends android.support.v7.widget.AppCompatButton {

    public FontButton ( Context context ) {
        super ( context );
    }

    public FontButton ( Context context, AttributeSet attrs ) {
        super ( context, attrs );
        setCustomFont ( context, attrs );
    }

    public FontButton ( Context context, AttributeSet attrs, int defStyle ) {
        super ( context, attrs, defStyle );
        setCustomFont ( context, attrs );
    }

    private void setCustomFont ( Context ctx, AttributeSet attrs ) {
        TypedArray a = ctx.obtainStyledAttributes ( attrs, R.styleable.FontButton );
        String customFont = a.getString ( R.styleable.FontButton_fontButton );
        setCustomFont ( ctx, "font/" + customFont + ".ttf" );
        a.recycle ( );
    }

    public void setCustomFont ( Context ctx, String asset ) {
        Typeface tf;
        try {
            tf = Typeface.createFromAsset ( ctx.getAssets ( ), asset );
        } catch ( Exception e ) {
            return;
        }

        setTypeface ( tf );
    }
}