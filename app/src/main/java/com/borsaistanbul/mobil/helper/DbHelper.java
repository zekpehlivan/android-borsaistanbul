
package com.borsaistanbul.mobil.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;


    private static final String DATABASE_NAME = "takip_list";


    private static final String TABLE_NAME = "list";


    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "kod";
    private static final String KEY_TIME = "time";


    public DbHelper ( Context context ) {
        super ( context, DATABASE_NAME, null, DATABASE_VERSION );
    }


    @Override
    public void onCreate ( SQLiteDatabase db ) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_TIME + " TEXT" + ")";
        db.execSQL ( CREATE_LOGIN_TABLE );
    }


    @Override
    public void onUpgrade ( SQLiteDatabase db, int oldVersion, int newVersion ) {

        db.execSQL ( "DROP TABLE IF EXISTS " + TABLE_NAME );


        onCreate ( db );
    }


    public void addItem ( String sembol, String time ) {
        SQLiteDatabase db = this.getWritableDatabase ( );
        ContentValues values = new ContentValues ( );
        values.put ( KEY_NAME, sembol );
        values.put ( KEY_TIME, time );
        db.insert ( TABLE_NAME, null, values );
        db.close ( );
    }

    public void removeItem ( String symbol ) {
        SQLiteDatabase db = this.getReadableDatabase ( );
        db.delete ( TABLE_NAME, KEY_NAME + "='" + symbol + "'", null );
        db.close ( );
    }

    public boolean isAdded ( String kod ) {
        SQLiteDatabase db = this.getReadableDatabase ( );
        String query = "SElECT * FROM " + TABLE_NAME + " WHERE " + KEY_NAME + "='" + kod + "'";
        Cursor cursor = db.rawQuery ( query, null );
        int count = cursor.getCount ( );
        db.close ( );
        cursor.close ( );
        return count > 0;
    }

    public boolean checkDate ( String date, String kod ) {
        SQLiteDatabase db = this.getReadableDatabase ( );
        String query = "SElECT * FROM " + TABLE_NAME + " WHERE " + KEY_NAME + "='" + kod + "'";
        Cursor c = db.rawQuery ( query, null );
        c.moveToFirst ( );
        String d = c.getString ( c.getColumnIndex ( "time" ) ).replace ( ":", "" );

        db.close ( );
        c.close ( );
        return date.replace ( ":", "" ).equals ( d );

    }

    public void UpdateTime ( String kod, String time ) {
        SQLiteDatabase db = this.getWritableDatabase ( );
        ContentValues val = new ContentValues ( );
        val.put ( KEY_TIME, time );
        db.update ( TABLE_NAME, val, "kod=" + kod, null );
        db.close ( );

    }

    public int getRowCount ( ) {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase ( );
        Cursor cursor = db.rawQuery ( countQuery, null );
        int rowCount = cursor.getCount ( );
        db.close ( );
        cursor.close ( );


        return rowCount;
    }

    public void resetTables ( ) {
        SQLiteDatabase db = this.getWritableDatabase ( );
        db.delete ( TABLE_NAME, null, null );
        db.close ( );
    }

}
