package com.borsaistanbul.mobil.helper;

import android.app.AlertDialog;
import android.content.Context;

import com.kaopiz.kprogresshud.KProgressHUD;

import dmax.dialog.SpotsDialog;

@SuppressWarnings("WeakerAccess")
public class LoaderDialog {
    KProgressHUD dialog;
    Context c;

    public LoaderDialog(Context c) {
        this.c = c;
        this.dialog = KProgressHUD.create(c)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
               // .setLabel("Lütfen Bekleyin...")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

    }

    public void show() {
        dialog.show();

    }

    public void dismiss() {
        dialog.dismiss();
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }
}

