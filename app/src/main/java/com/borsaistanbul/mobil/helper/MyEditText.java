package com.borsaistanbul.mobil.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import com.borsaistanbul.mobil.R;

public class MyEditText extends android.support.v7.widget.AppCompatEditText {
    private static final String TAG = "TextView";

    public MyEditText ( Context context ) {
        super ( context );
    }

    public MyEditText ( Context context, AttributeSet attrs ) {
        super ( context, attrs );
        setCustomFont ( context, attrs );
    }

    public MyEditText ( Context context, AttributeSet attrs, int defStyle ) {
        super ( context, attrs, defStyle );
        setCustomFont ( context, attrs );
    }

    private void setCustomFont ( Context ctx, AttributeSet attrs ) {
        TypedArray a = ctx.obtainStyledAttributes ( attrs, R.styleable.MyEditText );
        String customFont = a.getString ( R.styleable.MyEditText_fontEditText );
        setCustomFont ( ctx, "font/" + customFont + ".ttf" );
        a.recycle ( );
    }

    public void setCustomFont ( Context ctx, String asset ) {
        Typeface tf;
        try {
            tf = Typeface.createFromAsset ( ctx.getAssets ( ), asset );
        } catch ( Exception e ) {
            Log.e ( TAG, "Could not get typeface: " + e.getMessage ( ) );
            return;
        }

        setTypeface ( tf );
    }

}

