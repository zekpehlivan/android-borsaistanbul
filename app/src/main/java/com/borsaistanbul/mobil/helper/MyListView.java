package com.borsaistanbul.mobil.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;


public class MyListView extends ListView {
    private Context mContext;


    public MyListView(Context context, AttributeSet attrs) {
        super ( context, attrs );
        this.mContext = context;
    }

    public MyListView ( Context context ) {
        super ( context );
        this.mContext = context;
    }

    public MyListView ( Context context, AttributeSet attrs, int defStyle ) {
        super ( context, attrs, defStyle );
        this.mContext = context;
    }

    @Override
    public void onMeasure ( int widthMeasureSpec, int heightMeasureSpec ) {
        int expandSpec = MeasureSpec.makeMeasureSpec ( Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST );
        super.onMeasure ( widthMeasureSpec, expandSpec );
    }


}