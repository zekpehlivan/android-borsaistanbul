package com.borsaistanbul.mobil.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.borsaistanbul.mobil.R;

public class SpinnerDialog {
    private Dialog dialog;

    public SpinnerDialog ( Context ctx ) {
        dialog = new Dialog ( ctx );
        dialog.requestWindowFeature ( Window.FEATURE_NO_TITLE );
        dialog.getWindow ( ).setBackgroundDrawable ( new ColorDrawable ( android.graphics.Color.TRANSPARENT ) );
        dialog.setCancelable ( false );
        dialog.setContentView ( R.layout.spinner_layout );
        dialog.getWindow ( ).setDimAmount ( 0.0f );
    }

    public void show ( ) {
        dialog.show ( );
    }

    public void dismiss ( ) {
        dialog.dismiss ( );
    }

    public boolean isShown ( ) {
        return dialog.isShowing ( );
    }
}
