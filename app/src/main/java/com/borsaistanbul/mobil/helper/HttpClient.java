package com.borsaistanbul.mobil.helper;

import android.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class HttpClient {
    private static org.apache.http.client.HttpClient custHttpClient;
    private final String basicAuth = "Basic " + Base64.encodeToString ( "fikirsel-medya:dsaTnnmy!".getBytes ( ), Base64.NO_WRAP );


    private static org.apache.http.client.HttpClient getHttpClient() {
        if (custHttpClient == null) {
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

            HttpParams connManagerParams = new BasicHttpParams();

            HttpConnectionParams.setConnectionTimeout(connManagerParams, 600);
            HttpConnectionParams.setSoTimeout(connManagerParams, 600);
            ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(new BasicHttpParams(), schemeRegistry);

            custHttpClient = new DefaultHttpClient(cm, null);
            HttpParams para = custHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(para, (30 * 1000));
            HttpConnectionParams.setSoTimeout(para, (30 * 1000));
            ConnManagerParams.setTimeout(para, (30 * 1000));
        }
        return custHttpClient;
    }


    public String Get(String urlPostFix)
            throws Exception {
        BufferedReader in = null;
        try {
            org.apache.http.client.HttpClient client = getHttpClient();
            HttpGet request = new HttpGet(urlPostFix);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");
            request.setHeader ( "Authorization", basicAuth );
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuilder sb = new StringBuilder("");
            String line;
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line).append(NL);
            }
            in.close();
            return sb.toString();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

}
