package com.borsaistanbul.mobil.helper;

import android.content.Context;
import android.content.res.Resources;

import com.borsaistanbul.mobil.R;

import cz.msebera.android.httpclient.client.cache.Resource;


public class apiInit {
    private String apiUser, apiPass;
    private Context ctx;
    private Resources res;

    public apiInit(Context ctx) {
        this.ctx = ctx;
        this.res = ctx.getResources();
    }

    public String getApiUser() {
        return res.getString(R.string.apiUser);
    }

    public String getApiPass() {
        return res.getString(R.string.apiPass);
    }
}
