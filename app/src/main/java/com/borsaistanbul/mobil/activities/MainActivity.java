package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.borsaistanbul.mobil.BaseActivity;
import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.MenuAdapter;
import com.borsaistanbul.mobil.adapters.MenuItems;
import com.borsaistanbul.mobil.fragments.AnaSayfa;
import com.borsaistanbul.mobil.fragments.DashBoard;
import com.borsaistanbul.mobil.fragments.Haberler;
import com.borsaistanbul.mobil.fragments.Kap;
import com.borsaistanbul.mobil.fragments.Piyasalar;
import com.borsaistanbul.mobil.fragments.Sirketler;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.borsaistanbul.mobil.helper.NetworkInformation;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    LinearLayout menu;
    ImageView dashbord;
    ImageView piyasalar;
    ImageView sayfam;
    TextView duyuru;
    ImageView kap;
    ListView menuList;
    ImageView sirketler;
    MyTextView anasayfa;
    public MainActivity app;
    private FirebaseAnalytics analitik;
    boolean connected = true;
    MyTextView bilgiBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer = findViewById(R.id.drawer_layout);
        findViewById(R.id.buyutec).setOnClickListener(this);
        menu = findViewById(R.id.menu);
        menu.setOnClickListener(this);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        dashbord = findViewById(R.id.dashboard);
        piyasalar = findViewById(R.id.piyasalar);
        sayfam = findViewById(R.id.sayfam);
        duyuru = findViewById(R.id.duyuru);
        sirketler = findViewById(R.id.sirketler);
        anasayfa = findViewById(R.id.anasayfa);
        bilgiBar = findViewById(R.id.bilgiBar);
        kap = findViewById(R.id.kap);
        dashbord.setOnClickListener(this);
        piyasalar.setOnClickListener(this);
        sayfam.setOnClickListener(this);
        duyuru.setOnClickListener(this);
        kap.setOnClickListener(this);
        sirketler.setOnClickListener(this);
        anasayfa.setOnClickListener(this);
        menuList = findViewById ( R.id.menuList );
        menuList.setOnItemClickListener ( new AdapterView.OnItemClickListener ( ) {
            @Override
            public void onItemClick ( AdapterView <?> adapterView, View view, int i, long l ) {
                MenuItems menuItems = (MenuItems) adapterView.getItemAtPosition ( i );
                startActivity ( new Intent ( MainActivity.this, ContentViewer.class ).putExtra ( "id", menuItems.getId ( ) ) );
            }
        } );
        callAnaSayfa();

        app = this;
        analitik = FirebaseAnalytics.getInstance ( this );
        new MenuList ( ).execute ( );

    }

    private void callAnaSayfa() {
        Fragment anaSayfa = new DashBoard();
        ft = fm.beginTransaction();
        ft.replace(R.id.container, anaSayfa).commit();
        bilgiBar.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View view) {
        if (view == menu) {
            drawer.openDrawer(GravityCompat.START);

        }
        if (view.getId() == R.id.buyutec) {
            startActivity(new Intent(this, Search.class));
        }
        if (view == dashbord) {
            Fragment dashboard = new DashBoard();
            ft = fm.beginTransaction();
            ft.replace(R.id.container, dashboard).commit();
            analitik.setCurrentScreen(this, "Ekran", dashboard.getClass().getSimpleName());
            checkNetwork ( );
            bilgiBar.setVisibility(View.VISIBLE);

        }
        if (view == piyasalar) {
            Fragment piyasalar = new Piyasalar();
            ft = fm.beginTransaction();
            ft.replace(R.id.container, piyasalar).commit();
            analitik.setCurrentScreen(this, "Piyasalar", piyasalar.getClass().getSimpleName());
            checkNetwork ( );
            bilgiBar.setVisibility(View.VISIBLE);
        }
        if (view == sayfam) {
            Fragment sayfam = new AnaSayfa ( );
            ft = fm.beginTransaction();
            ft.replace ( R.id.container, sayfam ).commit ( );
            analitik.setCurrentScreen ( this, "Sayfam", sayfam.getClass ( ).getSimpleName ( ) );
            checkNetwork ( );
            bilgiBar.setVisibility(View.GONE);

        }
        if (view == duyuru) {
            Fragment duyuru = new Haberler();
            ft = fm.beginTransaction();
            ft.replace(R.id.container, duyuru).commit();
            analitik.setCurrentScreen(this, "Duyurular", sayfam.getClass().getSimpleName());
            checkNetwork ( );
            closeDrawer();
            bilgiBar.setVisibility(View.GONE);


        }
        if (view == kap) {
            Fragment kap = new Kap ( );
            ft = fm.beginTransaction();
            ft.replace ( R.id.container, kap ).commit ( );
            analitik.setCurrentScreen ( this, "Kap Liste", sayfam.getClass ( ).getSimpleName ( ) );
            checkNetwork ( );
            bilgiBar.setVisibility(View.GONE);

        }
        if (view == sirketler) {
            Fragment sirket = new Sirketler();
            ft = fm.beginTransaction();
            ft.replace(R.id.container, sirket).commit();
            analitik.setCurrentScreen(this, "Şirketler Liste", sayfam.getClass().getSimpleName());
            checkNetwork();
            bilgiBar.setVisibility(View.GONE);

        }
        if (view == anasayfa) {
            closeDrawer();
            callAnaSayfa();
        }
    }


    public void changeIcon(String page) {
        if (page.equals("dashboard")) {
            dashbord.setImageResource(R.drawable.btn_ana_sayfa_active);
        } else {
            dashbord.setImageResource(R.drawable.btn_ana_sayfa);

        }
        if (page.equals("piyasalar")) {
            piyasalar.setImageResource(R.drawable.btn_tab_menu_hisse_active);
        } else {
            piyasalar.setImageResource(R.drawable.btn_tab_menu_hisse);
        }
        if (page.equals("sayfam")) {
            sayfam.setImageResource(R.drawable.btn_tab_menu_sayfam_active);
        } else {
            sayfam.setImageResource(R.drawable.btn_tab_menu_sayfam);
        }
        if (page.equals("sirketler")) {
            sirketler.setImageResource(R.drawable.btn_tab_menu_sirket_active);
        } else {
            sirketler.setImageResource(R.drawable.btn_tab_menu_sirket);
        }
        if (page.equals("kap")) {
            kap.setImageResource(R.drawable.btn_tab_menu_kap_active);
        } else {
            kap.setImageResource(R.drawable.btn_tab_menu_kap);
        }
    }

    @SuppressLint ( "StaticFieldLeak" )
    private class MenuList extends AsyncTask <String, String, String> {

        @Override
        protected String doInBackground ( String... strings ) {
            HttpClient client = new HttpClient ( );
            String res = null;
            try {
                res = client.Get ( "https://bist.elluga.net/service.php?secure=MY_SECURITY_KEY&type=page_list" );
            } catch ( Exception e ) {
                e.printStackTrace ( );
            }
            return res;
        }

        @Override
        protected void onPostExecute ( String s ) {
            ArrayList <MenuItems> item = new ArrayList <> ( );
            if ( s != null ) {
                try {
                    JSONObject obj = new JSONObject ( s );
                    JSONArray pages = obj.getJSONArray ( "pages" );
                    for ( int i = 0 ; i < pages.length ( ) ; i++ ) {
                        String title = pages.getJSONObject ( i ).getString ( "page_title" );
                        String id = pages.getJSONObject ( i ).getString ( "page_id" );
                        item.add ( new MenuItems ( title, id ) );

                    }
                    MenuAdapter adapter = new MenuAdapter ( MainActivity.this, R.layout.menu_items, item );
                    menuList.setAdapter ( adapter );
                } catch ( JSONException e ) {
                    e.printStackTrace ( );
                }


            }
        }
    }

    public void dialog ( ) {
        AlertDialog.Builder dialog = new AlertDialog.Builder ( this );
        dialog.setTitle ( "Bağlantı Hatası" );
        dialog.setMessage ( "Lütfen bağlantı ayarlarınızı kontrol ediniz" );
        dialog.setPositiveButton ( "Tamam", new DialogInterface.OnClickListener ( ) {
            @Override
            public void onClick ( DialogInterface dialogInterface, int i ) {
                dialogInterface.dismiss ( );
            }
        } );
        dialog.show ( );
    }

    public void checkNetwork ( ) {
        if ( !NetworkInformation.isConnected ( this ) ) {
            dialog ( );
        }
    }

}

