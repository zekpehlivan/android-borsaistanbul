package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.BildirimAdapter;
import com.borsaistanbul.mobil.adapters.BildirimItems;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.MyListView;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class SirketDetay extends AppCompatActivity {
    MyTextView adres, posta, web, sektor;
    LinearLayout yetkililer, sirkeBilgiLoader, sirketBilgiContent;
    ImageView logo;
    MyListView listItem;
    private FirebaseAnalytics analitik;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_sirket_detay );
        Intent i = getIntent ( );
        String c = i.getStringExtra ( "c" );
        String unvan = i.getStringExtra ( "u" );
        ActionBar tb = getSupportActionBar ( );
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled ( true );
        tb.setBackgroundDrawable ( new ColorDrawable ( getResources ( ).getColor ( R.color.navColor ) ) );
        //  tb.setTitle(unvan);
        tb.setDisplayShowCustomEnabled ( true );
        tb.setCustomView ( R.layout.custom_title_bar );

        TextView text = findViewById ( R.id.action_bar_title );
        text.setTextSize ( 15f );
        text.setText ( unvan );
        sirketDetay(c);
        adres = findViewById ( R.id.adr );
        posta = findViewById ( R.id.posta );
        web = findViewById ( R.id.web );
        sektor = findViewById ( R.id.sektor );
        logo = findViewById ( R.id.logo );
        yetkililer = findViewById ( R.id.yetkililer );
        sirkeBilgiLoader = findViewById ( R.id.sirketBilgiLoader );
        sirketBilgiContent = findViewById ( R.id.sirketBilgiContent );
        listItem = findViewById ( R.id.listItem );
        listItem.setOnItemClickListener ( new AdapterView.OnItemClickListener ( ) {
            @Override
            public void onItemClick ( AdapterView <?> adapterView, View view, int i, long l ) {
                BildirimItems items = (BildirimItems) adapterView.getItemAtPosition ( i );
                startActivity ( new Intent ( SirketDetay.this, BildirimDetay.class )
                        .putExtra ( "unvan", items.getUnvan ( ) )
                        .putExtra ( "bid", items.getBildirimNo ( ) ) );
            }
        } );
        analitik = FirebaseAnalytics.getInstance ( this );
    }

    @Override
    public boolean onSupportNavigateUp ( ) {
        finish ( );
        return true;
    }

    public  void sirketDetay(String value){
        AsyncHttpClient client = new AsyncHttpClient(true,80,443);
        client.setConnectTimeout(20*10000);
        client.setTimeout(20*10000);
        client.setResponseTimeout(20*10000);
        client.get("http://service.elluga.net/api/servis/kap/talep/sirketozet/sirket/" + value + "/format/json", new AsyncHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
               if(responseBody!=null) {
                   try {
                       String strSektor = null;
                       JSONObject obj = new JSONObject ( new String(responseBody) );
                       String strAdres = null;
                       String strEposta = null;
                       String strWeb = null;
                       String strUnvan = null;
                       if ( !obj.getJSONArray ( "ozet_bilgileri" ).isNull ( 0 ) ) {
                           strAdres = obj.getJSONArray ( "ozet_bilgileri" ).getJSONObject ( 0 ).getString ( "bilgi_degeri" );

                       }
                       if ( !obj.getJSONArray ( "ozet_bilgileri" ).isNull ( 1 ) ) {
                           strEposta = obj.getJSONArray ( "ozet_bilgileri" ).getJSONObject ( 1 ).getString ( "bilgi_degeri" );
                       }
                       if ( !obj.getJSONArray ( "ozet_bilgileri" ).isNull ( 2 ) ) {
                           strWeb = obj.getJSONArray ( "ozet_bilgileri" ).getJSONObject ( 2 ).getString ( "bilgi_degeri" );
                       }
                       if ( !obj.getJSONArray ( "ozet_bilgileri" ).isNull ( 6 ) ) {
                           strSektor = obj.getJSONArray ( "ozet_bilgileri" ).getJSONObject ( 6 ).getString ( "bilgi_degeri" );
                       }

                       String strLogo = obj.getJSONObject ( "sirket_bilgileri" ).getString ( "sirket_logo" );
                       Picasso.with ( SirketDetay.this ).load ( strLogo ).into ( logo );
                       strUnvan = obj.getJSONObject ( "sirket_bilgileri" ).getString ( "sirket_adi" );
                       if ( !obj.isNull ( "yetkili_bilgileri" ) ) {
                           JSONArray arr = obj.getJSONArray ( "yetkili_bilgileri" );
                           for ( int i = 0 ; i < arr.length ( ) ; i++ ) {
                               JSONObject o = arr.getJSONObject ( i );
                               TableRow.LayoutParams params = new TableRow.LayoutParams ( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT );
                               LinearLayout.LayoutParams viewParam = new TableRow.LayoutParams ( ViewGroup.LayoutParams.MATCH_PARENT, 1 );
                               View view = new View ( SirketDetay.this );
                               view.setLayoutParams ( viewParam );
                               view.setBackgroundColor ( getResources ( ).getColor ( R.color.textLightGray ) );
                               MyTextView yetkiliTw = new MyTextView ( SirketDetay.this );
                               MyTextView gorevTw = new MyTextView ( SirketDetay.this );
                               yetkiliTw.setLayoutParams ( params );
                               yetkiliTw.setText ( o.getString ( "yetkili_adi" ) );
                               yetkiliTw.setCustomFont ( SirketDetay.this, getString ( R.string.UbuntuRegular ) );
                               yetkiliTw.setTextSize ( 15f );
                               yetkiliTw.setPadding ( spToPixel ( 10 ), spToPixel ( 10 ), spToPixel ( 10 ), spToPixel ( 10 ) );
                               gorevTw.setLayoutParams ( params );
                               gorevTw.setText ( o.getString ( "yetkili_gorevi" ) );
                               gorevTw.setTextSize ( 13f );
                               gorevTw.setCustomFont ( SirketDetay.this, getString ( R.string.UbuntuMedium ) );
                               gorevTw.setTextColor ( getResources ( ).getColor ( R.color.black ) );
                               gorevTw.setPadding ( spToPixel ( 10 ), 0, spToPixel ( 10 ), 0 );
                               yetkililer.addView ( yetkiliTw );
                               yetkililer.addView ( gorevTw );
                               yetkililer.addView ( view );
                           }

                       }

                       adres.setText ( strAdres );
                       posta.setText ( strEposta );
                       web.setText ( strWeb );
                       sektor.setText ( strSektor );
                       JSONArray array = obj.getJSONArray ( "son_bes_bildirim" );
                       ArrayList <BildirimItems> list = new ArrayList <> ( );
                       for ( int j = 0 ; j < array.length ( ) ; j++ ) {
                           JSONObject o = array.getJSONObject ( j );
                           String kod = o.getString ( "sirket_kodu" );
                           String unvan = o.getString ( "sirket_adi" );
                           String title = o.getString ( "bildirim_baslik" );
                           String date = o.getString ( "yayinlanma_tarihi_formatli" );
                           String bildirimNo = o.getString ( "bildirim_numarasi" );
                           SimpleDateFormat df = new SimpleDateFormat ( "dd.MM.yyyy HH:mm", new Locale ( "tr", "TR" ) );
                           SimpleDateFormat dateFormat = new SimpleDateFormat ( "dd MMMM", new Locale ( "tr", "TR" )  );
                           String saat = dateFormat.format ( df.parse ( date ) );

                           // String saat = df[1];
                           list.add ( new BildirimItems ( kod, saat, unvan, title, bildirimNo ) );
                       }
                       BildirimAdapter adapter = new BildirimAdapter ( SirketDetay.this, R.layout.kap_fragment_list_item, list );
                       listItem.setAdapter ( adapter );

                       sirkeBilgiLoader.setVisibility ( View.GONE );
                       sirketBilgiContent.setVisibility ( View.VISIBLE );
                       analitik.setCurrentScreen ( SirketDetay.this, "Şirket Detay - " + strUnvan, null );


                   } catch ( JSONException | ParseException e ) {
                       e.printStackTrace ( );
                   }

               }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }


    public int spToPixel ( int sp ) {
        int scaledDensity = (int) getResources ( ).getDisplayMetrics ( ).scaledDensity;
        return sp * scaledDensity;
    }
}
