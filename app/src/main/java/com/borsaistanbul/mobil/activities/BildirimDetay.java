package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.borsaistanbul.mobil.BaseActivity;
import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.FontButton;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

public class BildirimDetay extends BaseActivity implements View.OnClickListener {
    WebView web;
    FontButton btnHaber;
    FontButton btnPdf;
    String bid;
    String unvan;
    boolean ref = false;
    private FirebaseAnalytics analitik;

    @SuppressLint ( "SetJavaScriptEnabled" )
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_bildirim_detay );
        Intent i = getIntent ( );
        if ( i.getStringExtra ( "ref" ) != null ) {
            ref = true;
        }
        ;
        unvan = i.getStringExtra ( "unvan" );
        bid = i.getStringExtra ( "bid" );
        ActionBar tb = getSupportActionBar ( );
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled ( true );
        tb.setBackgroundDrawable ( new ColorDrawable ( getResources ( ).getColor ( R.color.navColor ) ) );
        //  tb.setTitle(unvan);
        tb.setDisplayShowCustomEnabled ( true );
        tb.setCustomView ( R.layout.custom_title_bar );

        TextView text = findViewById ( R.id.action_bar_title );
        text.setTextSize ( 15f );
        text.setText ( unvan );
        web = findViewById ( R.id.webContent );
        web.setWebChromeClient ( new WebChromeClient ( ) );
        web.setWebViewClient ( new WebViewClient ( ) {
            LoaderDialog dialog = new LoaderDialog ( BildirimDetay.this );

            @Override
            public void onPageStarted ( WebView view, String url, Bitmap favicon ) {
                dialog.show ( );

            }

            @Override
            public void onPageFinished ( WebView view, String url ) {

                dialog.dismiss ( );
            }
        } );
        web.getSettings ( ).setJavaScriptCanOpenWindowsAutomatically ( true );
        web.getSettings ( ).setJavaScriptEnabled ( true );
        web.getSettings ( ).setLoadWithOverviewMode ( true );
        web.getSettings ( ).setBuiltInZoomControls ( true );
        web.setScrollbarFadingEnabled ( true );
        web.getSettings ( ).setDisplayZoomControls ( false );
        String pdfurl = "https://www.kap.org.tr/tr/BildirimPdf/" + bid;
        web.loadUrl ( "http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfurl );
        btnHaber = findViewById ( R.id.btnHaber );
        btnPdf = findViewById ( R.id.btnPdf );
        btnPdf.setOnClickListener ( this );
        btnHaber.setOnClickListener ( this );
        analitik = FirebaseAnalytics.getInstance ( this );


    }

    @Override
    public boolean onSupportNavigateUp ( ) {
        if ( ref ) {
            Intent main = new Intent ( this, MainActivity.class );
            main.setFlags ( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity ( main );
            finish ( );
        } else {
            finish ( );

        }
        return true;
    }

    @Override
    public void onClick ( View view ) {
        if ( view == btnHaber ) {
            startActivity ( new Intent ( this, Browser.class ).putExtra ( "bid", bid ) );
        }
        if ( view == btnPdf ) {
            Intent target = new Intent ( Intent.ACTION_VIEW );
            target.setDataAndType ( Uri.parse ( "https://www.kap.org.tr/tr/BildirimPdf/" + bid ), "application/pdf" );
            target.setFlags ( Intent.FLAG_ACTIVITY_NO_HISTORY );
            startActivity ( target );
        }
    }

    @SuppressLint ( "StaticFieldLeak" )
    private class getWebContent extends AsyncTask <Object, Object, String> {
        String result;
        LoaderDialog dialog;

        @Override
        protected void onPreExecute ( ) {
            dialog = new LoaderDialog ( BildirimDetay.this );
            dialog.show ( );
        }

        @Override
        protected String doInBackground ( Object... objects ) {
            HttpClient client = new HttpClient ( );
            try {
                result = client.Get ( "http://service.elluga.net/api/servis/kap/talep/bildirimdetay/bildirim/" + bid + "/format/json" );
            } catch ( Exception e ) {
                e.printStackTrace ( );
            }
            return null;
        }

        @Override
        protected void onPostExecute ( String s ) {
            if ( result != null ) {
                try {
                    JSONObject obj = new JSONObject ( result );
                    String data = obj.getString ( "bildirim_icerik" );
                    unvan = obj.getString ( "sirket_adi" );
                    String konu = obj.getString ( "bildirim_konusu" );
                    data = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href='/assets/stylesheets/bundle.min.css?b=127860' ></head><body>" + data + "</body></html>";
                    web.loadData ( data, "text/html", "UTF-8" );
                    analitik.setCurrentScreen ( BildirimDetay.this, "Bildirim Detay - " + bid + "-" + konu, null );
                } catch ( JSONException e ) {
                    e.printStackTrace ( );
                }
            }
            dialog.dismiss ( );
        }
    }

    @Override
    public void onBackPressed ( ) {
        if ( ref ) {
            Intent main = new Intent ( this, MainActivity.class );
            main.setFlags ( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity ( main );
            finish ( );
        } else {
            finish ( );

        }
    }
}
