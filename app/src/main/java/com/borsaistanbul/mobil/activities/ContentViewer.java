package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

public class ContentViewer extends AppCompatActivity {
    private ActionBar tb;
    WebView wv;
    private FirebaseAnalytics analitik;

    @SuppressLint ( "SetJavaScriptEnabled" )
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_content_viewer );

        Intent i = getIntent ( );
        String id = i.getStringExtra ( "id" );
        tb = getSupportActionBar ( );
        assert tb != null;

        tb.setDisplayHomeAsUpEnabled ( true );
        tb.setBackgroundDrawable ( new ColorDrawable ( getResources ( ).getColor ( R.color.navColor ) ) );
        tb.setTitle ( "" );

        wv = findViewById ( R.id.contents );
        wv.setWebViewClient(new WebViewClient());
        wv.setWebChromeClient(new WebChromeClient());
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setAllowFileAccessFromFileURLs(false);
        wv.getSettings().setAllowUniversalAccessFromFileURLs(false);
        wv.setBackgroundColor(0);

        new getContent ( ).execute ( id );
        analitik = FirebaseAnalytics.getInstance ( this );

    }

    @Override
    public boolean onSupportNavigateUp ( ) {
        finish ( );
        return true;
    }

    @Override
    public void onBackPressed ( ) {
        finish ( );
    }

    private class getContent extends AsyncTask <String, String, String> {
        LoaderDialog dialog;

        @Override
        protected String doInBackground ( String... strings ) {
            HttpClient client = new HttpClient ( );
            String res = null;
            try {
                res = client.Get ( "https://bist.elluga.net/service.php?secure=MY_SECURITY_KEY&type=page_detail&id=" + strings[0] );
            } catch ( Exception e ) {
                e.printStackTrace ( );
            }
            return res;
        }

        @Override
        protected void onPreExecute ( ) {
            dialog = new LoaderDialog ( ContentViewer.this );
            dialog.show ( );
        }

        @Override
        protected void onPostExecute ( String s ) {
            if ( s != null ) {
                try {
                    JSONObject obj = new JSONObject ( s );

                    if (obj.getString ( "page_title" ).equals("Hakkımızda")){
                        wv.setBackgroundResource(R.drawable.bg_hakkimizda);
                    }else if (obj.getString ( "page_title" ).equals("Misyon ve Vizyon")){
                        wv.setBackgroundResource(R.drawable.bg_misyon);
                    }else if (obj.getString ( "page_title" ).equals("İletişim")){
                        wv.setBackgroundResource(R.drawable.bg_hakkimizda);
                    }else {
                        wv.setBackgroundResource(R.drawable.bg_misyon);
                    }

                    tb.setTitle ( obj.getString ( "page_title" ) );
                    wv.loadData ( obj.getString ( "page_text" ), "text/html", "UTF-8" );
                    analitik.setCurrentScreen ( ContentViewer.this, obj.getString ( "page_title" ), null );

                } catch ( JSONException e ) {
                    e.printStackTrace ( );
                }
            }
            dialog.dismiss ( );
        }
    }
}
