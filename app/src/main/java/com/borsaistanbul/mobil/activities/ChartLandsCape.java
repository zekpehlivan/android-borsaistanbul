package com.borsaistanbul.mobil.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.HistoryItems;
import com.borsaistanbul.mobil.helper.DoubleChartGestureListener;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ChartLandsCape extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    CombinedChart chart;
    BarChart barChart;
    String code;
    ArrayList<HistoryItems> list = new ArrayList<>();
    Spinner barSpinner, periodSpinner;
    String barArray[] = {"100", "200", "300"};
    String perArray[] = {"1 Dk.", "5 Dk.", "30 Dk.", "60 Dk.", "1 Gün", "Haftalık"};
    String perValArr[] = {"1", "5", "30", "60", "1440", "w"};
    boolean isLoaded = false;
    boolean dateWithTime = false;
    String ce;
    MyTextView tvTarih, tvAcilis, tvKapanis, tvYuksek, tvDusuk, tvHacim, tvUnvan, tvCode;
    LinearLayout detailText;
    ImageView share;
    String unvan;
    private File fullPath;
    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE

    };
    public static final int MULTIPLE_PERMISSIONS = 10;
    boolean hasBarData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_lands_cape);

        Intent i = getIntent();
        String s = i.getStringExtra("s");
        code = i.getStringExtra("c");
        ce = i.getStringExtra("ce");
        unvan = i.getStringExtra("unvan");
        chart = findViewById(R.id.chart);
        barChart = findViewById(R.id.chart2);
        barSpinner = findViewById(R.id.bar);
        periodSpinner = findViewById(R.id.period);
        tvTarih = findViewById(R.id.tvTarih);
        tvAcilis = findViewById(R.id.tvAcilis);
        tvKapanis = findViewById(R.id.tvKapanis);
        tvYuksek = findViewById(R.id.tvYuksek);
        tvDusuk = findViewById(R.id.tvDusuk);
        tvHacim = findViewById(R.id.tvHacim);
        detailText = findViewById(R.id.textDetail);
        tvUnvan = findViewById(R.id.unvan);
        tvCode = findViewById(R.id.kod);
        share = findViewById(R.id.share);
        share.setOnClickListener(this);

        barSpinner.setOnItemSelectedListener(this);
        periodSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> barAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, barArray);
        barAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        ArrayAdapter<String> perAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, perArray);
        perAdapter.setDropDownViewResource(R.layout.spinner_dropdown);

        barSpinner.setAdapter(barAdapter);
        periodSpinner.setAdapter(perAdapter);

        barSpinner.setSelection(0, false);
        periodSpinner.setSelection(4, false);

        tvUnvan.setText(unvan);
        tvCode.setText(code);

        getChartDefaultData(s);
        initBarChart();
        initChart();
        FirebaseAnalytics analitik = FirebaseAnalytics.getInstance(this);
        analitik.setCurrentScreen(ChartLandsCape.this, "Chart Full Screen - " + code + "-" + unvan, null);

    }

    private void getChartDefaultData(String s) {
        if (s != null) {
            if (list.size() > 0) {
                list.clear();
            }
            try {
                JSONArray arr;
                arr = new JSONArray(s);

                for (int i = 0; i < arr.length(); i++) {
                    JSONObject object = arr.getJSONObject(i);
                    list.add(new HistoryItems(
                            object.has("d") ? object.getString("d") : null,
                            object.has("o") ? object.getString("o") : null,
                            object.has("h") ? object.getString("h") : null,
                            object.has("l") ? object.getString("l") : null,
                            object.has("c") ? object.getString("c") : null,
                            object.has("v") ? object.getString("v").isEmpty() ? "0" : object.getString("v") : null,
                            object.has("a") ? object.getString("a") : null));
                    hasBarData = object.has("v") && object.has("v");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            String selectedValue = perValArr[periodSpinner.getSelectedItemPosition()];

            dateWithTime = !selectedValue.matches(".*[a-z].*") && Integer.parseInt(selectedValue) < 1140;
            fillChart();
            if (hasBarData) {
                fillBarChart();
                barChart.setVisibility(View.VISIBLE);
            } else {

                barChart.setVisibility(View.GONE);
            }
            isLoaded = true;

        }


    }

    private void initBarChart() {

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                float x = h.getX();
                float y = h.getY();
                Highlight high = new Highlight(x, 0, -1);
                high.setDataIndex(0);
                chart.highlightValue(high, false);

                getDetailText(e.getX());
            }

            @Override
            public void onNothingSelected() {

            }
        });
        barChart.setDrawGridBackground(false);
        barChart.getDescription().setEnabled(false);
        barChart.setOnChartGestureListener(new DoubleChartGestureListener(barChart, new Chart[]{chart}));
        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleYEnabled(false);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.setPinchZoom(false);
        barChart.getAxisRight().setEnabled(false);
        barChart.invalidate();


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(true);
        leftAxis.setAxisMinimum(0);
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @SuppressLint("DefaultLocale")
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String numberString = "";
                int val = (int) value;
                int count = val / 1000000000;
                if (count > 0) {
                    numberString = String.format("%dB", count);
                    return numberString;

                }
                count = val / 1000000;
                if (count > 0) {
                    numberString = String.format("%dM", count);
                    return numberString;

                }
                count = val / 1000;
                if (count > 0) {
                    numberString = String.format("%dK", count);
                    return numberString;

                }
                if (count == 0) {
                    numberString = String.format("%d", count);
                    return numberString;

                }
                return numberString;
            }
        });

        XAxis xAxis = barChart.getXAxis();
        xAxis.setEnabled(false);


    }

    private void getDetailText(float x) {
        HistoryItems items = list.get((int) x);
        /*String dusuk = String.format ( "<Big >%s</Big>", formatText ( items.getL ( ), 3 ) );
        String yuksek = String.format ( "<Big >%s</Big>", formatText ( items.getH ( ), 3 ) );
        String acilis = String.format ( "<Big >%s</Big>", formatText ( items.getO ( ), 3 ) );
        String kapanis = String.format ( "<Big >%s</Big>", formatText ( items.getC ( ), 3 ) );
        String hacim = String.format ( "<Big >%s</Big>", FormatCurrency ( Double.parseDouble ( items.getV ( ) ) ) ); String dusuk = String.format ( "<Big >%s</Big>", formatText ( items.getL ( ), 3 ) );
        */
        detailText.setVisibility(View.VISIBLE);
        String dusuk = formatText(items.getL(), 3);
        String yuksek = formatText(items.getH(), 3);
        String acilis = formatText(items.getO(), 3);
        String kapanis = formatText(items.getC(), 3);
        String hacim = items.getV() != null ? FormatCurrency(Double.parseDouble(items.getV())) : "0";
        SimpleDateFormat df;
        if (dateWithTime) {
            df = new SimpleDateFormat("dd.MM.yyyy HH:mm", new Locale("tr", "TR"));

        } else {
            df = new SimpleDateFormat("dd.MM.yyyy", new Locale("tr", "TR"));

        }
        long timeStamp = Long.parseLong(items.getD());

        String tarih = df.format(timeStamp);

        String text = String.format("<html><body><small>Tarih: %1$s &nbsp;&nbsp;Açılış: %2$s &nbsp;&nbsp;Kapanış: %3$s <font color=" + getResources().getColor(R.color.textGreenColor) + "> &nbsp;&nbsp;Yüksek: %4$s</font> <font color=red> &nbsp;&nbsp;Düşük: %5$s </font>&nbsp;&nbsp;Hacim: %6$s</small> </body><html>", tarih, acilis, kapanis, yuksek, dusuk, hacim);
        tvTarih.setText(Html.fromHtml(tarih));
        tvAcilis.setText(Html.fromHtml(acilis));
        tvKapanis.setText(Html.fromHtml(kapanis));
        tvYuksek.setText(Html.fromHtml(yuksek));
        tvDusuk.setText(Html.fromHtml(dusuk));
        tvHacim.setText(Html.fromHtml(hacim));

    }

    public void fillBarChart() {
        ArrayList<BarEntry> arrayList = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            HistoryItems item = list.get(i);
            if (item.getV() != null) {
                DecimalFormat dFormat = new DecimalFormat("0.00##");
                float val = 0;
                try {
                    Number d = dFormat.parse(item.getV());
                    val = d.floatValue();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                arrayList.add(new BarEntry(i, val));
            }


        }
        Legend l = barChart.getLegend();
        l.setEnabled(false);

        BarDataSet set1 = new BarDataSet(arrayList, code);
        set1.setDrawIcons(false);
        set1.setDrawValues(false);
        set1.setHighlightEnabled(true);
        set1.setHighLightColor(getResources().getColor(R.color.textGreenColor));
        set1.setColor(getResources().getColor(R.color.textRedColor));
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setValueTypeface(Typeface.createFromAsset(this.getAssets(), "font/Ubuntu-Regular.ttf"));
        BarData data = new BarData(set1);
        barChart.setData(data);
        data.notifyDataChanged();
        barChart.invalidate();
    }

    public void fillChart() {
        CombinedData data = new CombinedData();
        data.setData(hisse());
        data.setData(volume());
        chart.setDrawOrder(new CombinedChart.DrawOrder[]{CombinedChart.DrawOrder.CANDLE, CombinedChart.DrawOrder.LINE});
        chart.setData(data);
        chart.invalidate();
    }

    private void initChart() {
        chart.setDrawGridBackground(false);
        chart.setOnChartGestureListener(new DoubleChartGestureListener(chart, new Chart[]{barChart}));
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (hasBarData) {
                    barChart.highlightValue(h);

                }
                getDetailText(e.getX());

            }

            @Override
            public void onNothingSelected() {

            }
        });
        chart.getDescription().setEnabled(false);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleXEnabled(true);
        chart.setScaleYEnabled(true);
        chart.setPinchZoom(true);
        chart.getAxisRight().setEnabled(false);
        chart.invalidate();

        Legend l = chart.getLegend();
        l.setEnabled(false);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setLabelCount(7, false);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                double d = (double) value;
                return String.format(Locale.getDefault(), "%.3f", d);
            }
        });

        XAxis xAxis = chart.getXAxis();
        xAxis.setTypeface(Typeface.createFromAsset(getAssets(), "font/Ubuntu-Regular.ttf"));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularityEnabled(true);

        xAxis.setGranularity(1f);

    }

    public CandleData hisse() {

        ArrayList<CandleEntry> arrayList = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();


        for (int i = 0; i < list.size(); i++) {
            HistoryItems item = list.get(i);
            SimpleDateFormat df;
            if (dateWithTime) {
                df = new SimpleDateFormat("dd.MM.yyyy HH:mm ", new Locale("tr", "TR"));
            } else {
                df = new SimpleDateFormat("dd.MM.yyyy ", new Locale("tr", "TR"));

            }
            String date = df.format(Long.parseLong(item.getD()==null?"0":item.getD()));
            dates.add(date);
            float high = Float.parseFloat(item.getH()==null?"0":item.getH());
            float low = Float.parseFloat(item.getL()==null?"0":item.getL());
            float open = Float.parseFloat(item.getO()==null?"0":item.getO());
            float close = Float.parseFloat(item.getC()==null?"0":item.getC());
            arrayList.add(new CandleEntry(i, high, low, open, close));

        }

        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(dates));

        CandleDataSet set1 = new CandleDataSet(arrayList, code);
        set1.setDrawIcons(false);
        set1.setHighlightEnabled(false);
        set1.setShadowColor(Color.DKGRAY);
        set1.setDecreasingColor(Color.RED);
        set1.setDecreasingPaintStyle(Paint.Style.FILL_AND_STROKE);
        set1.setIncreasingColor(Color.rgb(122, 242, 84));
        set1.setIncreasingPaintStyle(Paint.Style.FILL_AND_STROKE);
        set1.setNeutralColor(Color.BLUE);
        set1.setValueTypeface(Typeface.createFromAsset(this.getAssets(), "font/Ubuntu-Regular.ttf"));
        return new CandleData(set1);
    }

    private LineData volume() {
        ArrayList<Entry> arrayList = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            HistoryItems item = list.get(i);
            SimpleDateFormat df;
            if (dateWithTime) {
                df = new SimpleDateFormat("dd.MM.yyyy HH:mm ", new Locale("tr", "TR"));
            } else {
                df = new SimpleDateFormat("dd.MM.yyyy ", new Locale("tr", "TR"));

            }
            String date = df.format(Long.parseLong(item.getD()));
            dates.add(date);
            long tr = Long.parseLong(item.getD()==null?"0":item.getD());
            float val = Float.parseFloat(item.getC()==null?"0":item.getC());
            arrayList.add(new Entry(i, val));


        }
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(dates));
        LineData d = new LineData();
        LineDataSet set1 = new LineDataSet(arrayList, code);
        set1.setColor(getResources().getColor(R.color.textRedColor));
        set1.setLineWidth(1f);
        set1.setMode(LineDataSet.Mode.LINEAR);
        set1.setDrawValues(false);
        set1.setDrawCircles(false);
        set1.setDrawCircleHole(false);
        set1.setValueTypeface(Typeface.createFromAsset(this.getAssets(), "font/Ubuntu-Regular.ttf"));

        d.addDataSet(set1);
        return d;
    }

    public String FormatCurrency(double cur) {
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("tr", "TR"));
        return nf.format(cur);

    }

    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Çıkmak için telefonuzu dik konuma getiriniz", Toast.LENGTH_SHORT).show();
        //finish ();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (isLoaded) {
            // detayText.setText ( null );
            detailText.setVisibility(View.INVISIBLE);
            new getChartData().execute();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        if (view == share) {
         /*  if ( ActivityCompat.checkSelfPermission ( this, Manifest.permission.WRITE_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED ) {
                checkPermissions ( );
                return;
            }
            Share ( );*/
            Bitmap screenshot = takeScreenshot();
            shareNoSave(screenshot);

        }
    }

    private void Share() {
        Bitmap bitmap = takeScreenshot();
        saveBitmap(bitmap);
        shareIt();
    }

    public Bitmap takeScreenshot() {
        // View rootView = findViewById ( android.R.id.content ).getRootView ( );
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", new Locale("tr", "TR"));
        String timeStamp = df.format(c.getTime());
        File imagePath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/BorsaIstanbul");
        String imageName = "/screenshot-" + code + "-" + timeStamp + ".png";
        fullPath = new File(imagePath + imageName);
        if (!imagePath.exists()) {
            imagePath.mkdir();
        }
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath + imageName);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

    private void shareIt() {
        Uri uri = Uri.fromFile(fullPath);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        // String shareBody = "In Tweecher, My highest score with screen shot";
        //   sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Tweecher score");
        //sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);

        startActivity(Intent.createChooser(sharingIntent, "Şununla paylaş"));
    }

    @SuppressLint("StaticFieldLeak")
    private class getChartData extends AsyncTask<String, String, String> {
        String p1;
        String p2;

        @Override
        protected void onPreExecute() {
            int pindex = periodSpinner.getSelectedItemPosition();
            this.p1 = perValArr[pindex];
            this.p2 = barSpinner.getSelectedItem().toString();
            Log.i("val:", "p1 :" + p1 + " p2: " + p2);
        }


        @Override
        protected String doInBackground(String... strings) {
            HttpClient client = new HttpClient();
            String res = null;
            try {
                res = client.Get("https://cloud.foreks.com/historical-service/intraday/code/" + ce + "/period/" + p1 + "/last/" + p2);
                //  res = client.Get ( "https://cloud.foreks.com/historical-service/intraday/code/"+c+"/period/1440/last/30" );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                getChartDefaultData(s);
            }
        }
    }

    private void checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getBaseContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Share();
                }
            }
        }
    }

    void shareNoSave(Bitmap bitmap) {
        String fileName = "share.jpg";
        File dir = new File(getCacheDir(), "images");
        dir.mkdirs();
        File file = new File(dir, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", file);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", new Locale("tr", "TR"));
        String timeStamp = df.format(c.getTime());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        intent.setDataAndType(uri, getContentResolver().getType(uri));
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        // intent.putExtra(Intent.EXTRA_TEXT, code+"-"+timeStamp+"");
        intent.putExtra(Intent.EXTRA_STREAM, uri);

        try {
            startActivity(Intent.createChooser(intent, "Hisse görüntüsü paylaş"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No App Available", Toast.LENGTH_SHORT).show();
        }
    }
}
