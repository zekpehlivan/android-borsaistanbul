package com.borsaistanbul.mobil.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.borsaistanbul.mobil.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_splash );

        Thread t = new Thread ( ) {
            @Override
            public void run ( ) {
                try {
                    sleep ( 5000 );
                    startActivity ( new Intent ( Splash.this, MainActivity.class ) );
                    finish ( );
                } catch ( InterruptedException e ) {
                    e.printStackTrace ( );
                }
            }
        };
        t.start ( );
    }
}
