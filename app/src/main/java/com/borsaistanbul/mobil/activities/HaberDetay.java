package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.borsaistanbul.mobil.BaseActivity;
import com.borsaistanbul.mobil.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

public class HaberDetay extends BaseActivity {
    ImageView resim;
    WebView content;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haber_detay);
        ActionBar tb = getSupportActionBar();
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled(true);
        tb.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.navColor)));
        tb.setTitle("");
        Intent i = getIntent();
        String img = i.getStringExtra("img");
        String text = i.getStringExtra("text");
        content = findViewById(R.id.hbrContent);
        resim = findViewById(R.id.hbrImg);
        content.setWebViewClient(new WebViewClient());
        content.setWebChromeClient(new WebChromeClient());
        content.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        content.getSettings().setJavaScriptEnabled(true);
        content.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        content.loadData(text, "text/html", "UTF-8");
        Picasso.with(this).load(img).into(resim);
        FirebaseAnalytics analitik = FirebaseAnalytics.getInstance ( this );
        analitik.setCurrentScreen ( this, "Haber Detay - " + i.getStringExtra ( "title" ), null );

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed ( ) {
        finish ( );
    }
}
