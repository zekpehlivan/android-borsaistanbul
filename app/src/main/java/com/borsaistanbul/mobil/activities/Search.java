package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.borsaistanbul.mobil.BaseActivity;
import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.HisseSearchAdapter;
import com.borsaistanbul.mobil.adapters.HisseSearchItems;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyEditText;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.borsaistanbul.mobil.helper.apiInit;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import com.google.common.collect.ComparisonChain;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class Search extends BaseActivity implements View.OnClickListener, TextWatcher {
    LinearLayout close;
    ArrayList<HisseSearchItems> list = new ArrayList<>();
    ArrayList<HisseSearchItems> filteredList = new ArrayList<>();
    HisseSearchAdapter adapter;
    ListView listItem;
    MyEditText query;
    LoaderDialog dialog;
    DbHelper helper;
    List<String> isAdded;
    MyTextView title;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hisse_ekle);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        adapter = new HisseSearchAdapter(this, R.layout.hisse_ekle_list_item, filteredList);
        isAdded = new ArrayList<>();
        helper = new DbHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery("Select * from list", null);
        while (c.moveToNext()) {
            isAdded.add(c.getString(c.getColumnIndex("kod")));
        }
        db.close();
        c.close();
        fillList();

        listItem = findViewById(R.id.listItem);
        listItem.setAdapter(adapter);
        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HisseSearchItems anasayfaItems = (HisseSearchItems) adapterView.getItemAtPosition(i);
                boolean hisse = anasayfaItems.getC().contains("E.BIST");
                Intent c = new Intent(Search.this, HisseDetay.class);
                if (hisse) {
                    c.putExtra("hisse", "e");
                }
                c.putExtra("c", anasayfaItems.getC());
                startActivity(c);
            }
        });

        query = findViewById(R.id.query);
        query.addTextChangedListener(this);
        FirebaseAnalytics analitik = FirebaseAnalytics.getInstance(this);
        analitik.setCurrentScreen(this, "Ürün Arama", null);
        title = findViewById(R.id.title);
        title.setText("Arama");
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String text = query.getText().toString().toUpperCase().trim()
                .replace("İ", "I")
                .replace("Ş", "S")
                .replace("Ü", "U")
                .replace("Ö", "O")
                .replace("Ç", "C")
                .replace("Ğ", "G")
                .replace("i", "I")
                .replace("ş", "S")
                .replace("ç", "C")
                .replace("ö", "O")
                .replace("ü", "U")
                .replace("ğ", "G");
        String txtUnvan = query.getText().toString().toUpperCase().trim();
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
        if (text.length() > 0) {
            //  filteredList.clear();
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getSembol().contains(text)) {

                    filteredList.add(list.get(j));
                } else {
                    String item = list.get(j).getName().toUpperCase().trim()
                            .replace("İ", "I")
                            .replace("Ş", "S")
                            .replace("Ü", "U")
                            .replace("Ö", "O")
                            .replace("Ç", "C")
                            .replace("Ğ", "G")
                            .replace("i", "I")
                            .replace("ş", "S")
                            .replace("ç", "C")
                            .replace("ö", "O")
                            .replace("ü", "U")
                            .replace("ğ", "G");

                    if (item.contains(text)) {
                        filteredList.add(list.get(j));
                    }
                }
            }
            sortList();
            adapter.notifyDataSetChanged();

        } else {
            displayItems();

        }
    }

    private void sortList() {
        Collections.sort(filteredList, new Comparator<HisseSearchItems>() {
            @Override
            public int compare(HisseSearchItems s1, HisseSearchItems s2) {
                return ComparisonChain.start().compare(s1.getOrd(), s2.getOrd()).result();
            }
        });
        adapter.notifyDataSetChanged();
    }


    private void fillList() {
        final ArrayList<HisseSearchItems> hisseler = new ArrayList<>();
        final ArrayList<HisseSearchItems> viop = new ArrayList<>();
        final AsyncHttpClient client = new AsyncHttpClient();
        apiInit api = new apiInit(this);
        helper = new DbHelper(this);
        client.setBasicAuth(api.getApiUser(), api.getApiPass());
        list.clear();
        client.get(getResources().getString(R.string.hisseEkleApi), new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {

                helper = new DbHelper(Search.this);
                dialog = new LoaderDialog(Search.this);
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {


                String str;
                if (responseBody != null) {

                    str = new String(responseBody);


                    if (str.length() > 0) {
                        JsonArray val = Json.parse(str).asArray();
                        for (JsonValue item : val) {
                            // Toast.makeText(Search.this,item.asObject().getString("legacyCode",null), Toast.LENGTH_SHORT).show();
                            String sembol = item.asObject().getString("legacyCode", null);
                            String saat = item.asObject().getString("time", null);
                            String name = item.asObject().getString("description", null);
                            String c = item.asObject().getString("c", null);
                            String order = "0";

                            hisseler.add(new HisseSearchItems(sembol, saat, name, c, order));
                        }

                    }
                }

                client.get(getResources().getString(R.string.forexSozlesmelerApi) + getResources().getString(R.string.forex_fields), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        if(responseBody!=null){
                            String result = new String(responseBody);
                            try {
                                JSONArray jsonArray = new JSONArray(result);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String sembol = object.getString("legacyCode");
                                    String saat = object.getString("time");
                                    String name = object.getString("description");
                                    String c = object.getString("c");
                                    viop.add(new HisseSearchItems(sembol,saat,name,c,"0"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        list.addAll(hisseler);
                        list.addAll(viop);
                        displayItems();
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        if(dialog.isShowing()){
                            dialog.dismiss();
                        }
                    }
                });


            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
            }


        });

    }

    private void displayItems() {
        ArrayList<HisseSearchItems> gecici = new ArrayList<>();
        filteredList.clear();
        for (HisseSearchItems items : list) {
            String order = items.getC();
            if (isAdded.contains(items.getC())) {

                order = "9999";
            }
            filteredList.add(new HisseSearchItems(items.getSembol(), items.getSaat(), items.getName(), items.getC(), order));


        }

        sortList();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
      recreate();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

