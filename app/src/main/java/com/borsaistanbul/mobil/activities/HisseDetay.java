package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.BildirimAdapter;
import com.borsaistanbul.mobil.adapters.BildirimItems;
import com.borsaistanbul.mobil.adapters.HistoryItems;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.MyListView;
import com.borsaistanbul.mobil.helper.MyMarkerView;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import cz.msebera.android.httpclient.Header;

public class HisseDetay extends AppCompatActivity implements OnChartGestureListener, OnChartValueSelectedListener, View.OnClickListener {
    private MyTextView fiili_dolasim, pd_dd, sd_net_kar, pd_tl, pd_usd, unvan, kod, son, gunluk_degisim, gunluk_degisim_orani, gunluk_degisim_oran, time, alis, satis, adet, hacim, onceki_gun_kapanis, gun_acilis, gun_dusuk, gun_yuksek, haftalik_dusuk, haftalik_yuksek;
    private MyTextView haftalik_degisim, aylik_dusuk, aylik_yuksek, aylik_degisim, yillik_dusuk, yillik_yuksek, yillik_degisim;
    private MyTextView adres, posta, web, sektor;
    private LinearLayout hisseDetayLoader, hisseDetayContent, yetkililer, sirkeBilgiLoader, sirketBilgiContent;
    private ImageView logo, ekle;
    private MyListView listItem;
    private LineChart chart;
    private String code, saat, c;
    private LinearLayout back;
    private DbHelper helper;
    private String resource;
    private boolean chartLoaded = false;
    private String strFirmaAdi;
    boolean isHisse = true;
    boolean noData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_custom);
        Intent i = getIntent();
        c = i.getStringExtra("c");
        isHisse = i.getStringExtra("hisse") != null;
        back = findViewById(R.id.back);
        unvan = findViewById(R.id.unvan);
        kod = findViewById(R.id.kod);
        son = findViewById(R.id.son);
        gunluk_degisim = findViewById(R.id.gunluk_degisim);
        gunluk_degisim_orani = findViewById(R.id.gunluk_degisim_orani);
        gunluk_degisim_oran = findViewById(R.id.gunluk_degisim_oran);
        fiili_dolasim = findViewById(R.id.fiili_dolasim);
        pd_dd = findViewById(R.id.pd_dd);
        sd_net_kar = findViewById(R.id.sd_net_kar);
        pd_tl = findViewById(R.id.pd_tl);
        pd_usd = findViewById(R.id.pd_usd);
        time = findViewById(R.id.time);
        alis = findViewById(R.id.alis);
        satis = findViewById(R.id.satis);
        adet = findViewById(R.id.adet);
        hacim = findViewById(R.id.hacim);
        onceki_gun_kapanis = findViewById(R.id.onceki_gun_kapanis);
        gun_acilis = findViewById(R.id.gun_acilis);
        gun_dusuk = findViewById(R.id.gunluk_dusuk);
        gun_yuksek = findViewById(R.id.gunluk_yuksek);
        haftalik_dusuk = findViewById(R.id.haftalik_dusuk);
        haftalik_yuksek = findViewById(R.id.haftalik_yuksek);
        haftalik_degisim = findViewById(R.id.haftalik_degisim);
        aylik_dusuk = findViewById(R.id.aylik_dusuk);
        aylik_yuksek = findViewById(R.id.aylik_yuksek);
        aylik_degisim = findViewById(R.id.aylik_degisim);
        yillik_dusuk = findViewById(R.id.yillik_dusuk);
        yillik_yuksek = findViewById(R.id.yillik_yuksek);
        yillik_degisim = findViewById(R.id.yillik_degisim);
        hisseDetayLoader = findViewById(R.id.hisseDetayLoader);
        hisseDetayContent = findViewById(R.id.hisseDetayContent);
        adres = findViewById(R.id.adr);
        posta = findViewById(R.id.posta);
        web = findViewById(R.id.web);
        sektor = findViewById(R.id.sektor);
        logo = findViewById(R.id.logo);
        yetkililer = findViewById(R.id.yetkililer);
        sirkeBilgiLoader = findViewById(R.id.sirketBilgiLoader);
        sirketBilgiContent = findViewById(R.id.sirketBilgiContent);
        listItem = findViewById(R.id.listItem);

        back.setOnClickListener(this);
        chart = findViewById(R.id.chart);
        new getHisseDetay().execute(c);
        new getChartData().execute(c);

        helper = new DbHelper(this);
        ekle = findViewById(R.id.ekle);
        FirebaseAnalytics analitik = FirebaseAnalytics.getInstance(this);

        analitik.setCurrentScreen(HisseDetay.this, "Hisse Detay - " + c, null);


        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BildirimItems items = (BildirimItems) adapterView.getItemAtPosition(i);
                startActivity(new Intent(HisseDetay.this, BildirimDetay.class)
                        .putExtra("unvan", items.getUnvan())
                        .putExtra("bid", items.getBildirimNo()));
            }
        });
        ekle.setOnClickListener(this);


    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (chartLoaded && !noData) {
                startActivity(new Intent(this, ChartLandsCape.class).putExtra("s", resource).putExtra("c", code).putExtra("ce", c).putExtra("unvan", strFirmaAdi));
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            finish();
        }
        if (view == ekle) {
            boolean varmi = helper.isAdded(c);
            if (varmi) {
                helper.removeItem(c);
                ekle.setImageResource(R.drawable.btn_kaynak_ekle);

            } else {
                helper.addItem(c, saat);
                ekle.setImageResource(R.drawable.btn_kaynak_ekle_active);
            }
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @SuppressLint("StaticFieldLeak")
    private class getHisseDetay extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            String url = getResources().getString(R.string.forexHisseDetayApi) + "&c=" + strings[0];
            HttpClient client = new HttpClient();
            String res = null;
            try {
                res = client.Get(url);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return res;
        }

        @SuppressLint({"SetTextI18n", "DefaultLocale"})
        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                try {
                    JSONArray arr = new JSONArray(s);
                    JSONObject obj = arr.getJSONObject(0);
                    String strUnvan = obj.getString("description");
                    String strKod = clearCode(obj.getString("legacyCode"));
                    String strSon = obj.getString("last");
                    String strGunlukDegisim = obj.getString("dailyChange");
                    String strGunlukDegisimOrani = obj.getString("dailyChangePercentage");
                    String strTime = (obj.has("time")) ? obj.getString("time") : "000000";
                    String strAlis = (obj.has("bidPrice1")) ? obj.getString("bidPrice1") : "0";
                    String strSatis = (obj.has("askPrice1")) ? obj.getString("askPrice1") : "0";
                    strAlis = strAlis != null ? strAlis : obj.has("bid") ? obj.getString("bid") : "0";
                    strSatis = strSatis != null ? strAlis : obj.has("ask") ? obj.getString("ask") : "0";
                    double strAdet = obj.has("dailyAmount") ? obj.getLong("dailyAmount") : 0;
                    long strHacim = obj.has("dailyVolume") ? obj.getLong("dailyVolume") : 0;
                    String strOncekiKapanis = obj.has("previousDayClose") ? obj.getString("previousDayClose") : "0";
                    String strGunAcilis = obj.has("dailyOpen") ? obj.getString("dailyOpen") : "0";
                    String strGunlukDusuk = obj.has("dailyLowest") ? obj.getString("dailyLowest") : "0";
                    String strGunYuksek = obj.getString("dailyHighest");
                    String strHaftalikDusuk = obj.has("weeklyLow") ? obj.getString("weeklyLow") : "0";
                    String strHaftalikYuksek = obj.has("weeklyHigh") ? obj.getString("weeklyHigh") : "0";
                    String strHaftalikDegisim = obj.has("weeklyChangePercentage") ? obj.getString("weeklyChangePercentage") : "0";
                    String strAylikDusuk = obj.has("monthlyLow") ? obj.getString("monthlyLow") : "0";
                    String strAylikYuksek = obj.has("monthlyHigh") ? obj.getString("monthlyHigh") : "0";
                    String strAyllikDegisim = obj.has("monthlyChangePercentage") ? obj.getString("monthlyChangePercentage") : "0";
                    String strYillikDusuk = obj.has("yearlyLow") ? obj.getString("yearlyLow") : "0";
                    String strYillikYuksek = obj.has("yearlyHigh") ? obj.getString("yearlyHigh") : "0";
                    String strYillikDegisim = obj.has("yearlyChangePercentage") ? obj.getString("yearlyChangePercentage") : "0";
                    double strEnSnNetKar = obj.has("lastPeriodProfit") ? obj.getLong("lastPeriodProfit") : 0;
                    double strFiiliDolasim = obj.has("freeFloatRate") ? obj.getLong("freeFloatRate") : 0;
                    double netCapital = obj.has("netCapital") ? obj.getLong("netCapital") : 0;
                    double capital = obj.has("capital") ? obj.getLong("capital") : 0;
                    double usdLast = new getUsdLast().execute().get();
                    pd_dd.setText(String.format("%,.2f", Double.parseDouble(strSon) / (netCapital / capital)));
                    sd_net_kar.setText(FormatCurrency(strEnSnNetKar));
                    fiili_dolasim.setText(String.format("%,.2f", strFiiliDolasim));
                    pd_tl.setText(FormatCurrency(capital * Double.parseDouble(strSon)));
                    pd_usd.setText(String.format("$%,.2f", (capital / usdLast) * Double.parseDouble(strSon)));
                    strFirmaAdi = strUnvan;
                    unvan.setText(strUnvan);
                    kod.setText("( " + strKod + " )");
                    son.setText(formatText(strSon, 2));
                    gunluk_degisim.setText(formatText(strGunlukDegisim, 2));
                    gunluk_degisim_orani.setText("(" + formatText(strGunlukDegisimOrani, 2) + "%)");
                    gunluk_degisim_oran.setText(formatText(strGunlukDegisimOrani, 2) + "%");
                    gunluk_degisim_oran.setTextColor(setColor(strGunlukDegisimOrani));
                    time.setText(formatTime(strTime));
                    alis.setText(formatText(strAlis, 2));
                    satis.setText(formatText(strSatis, 2));
                    adet.setText(String.format("%,.0f", strAdet));
                    hacim.setText(FormatCurrency(strHacim));
                    onceki_gun_kapanis.setText(formatText(strOncekiKapanis, 2));
                    gun_acilis.setText(formatText(strGunAcilis, 2));
                    gun_dusuk.setText(formatText(strGunlukDusuk, 2));
                    gun_yuksek.setText(formatText(strGunYuksek, 2));
                    haftalik_dusuk.setText(formatText(strHaftalikDusuk, 2));
                    haftalik_yuksek.setText(formatText(strHaftalikYuksek, 2));
                    haftalik_degisim.setText(formatText(strHaftalikDegisim, 2) + "%");
                    haftalik_degisim.setTextColor(setColor(strHaftalikDegisim));
                    aylik_dusuk.setText(formatText(strAylikDusuk, 2));
                    aylik_yuksek.setText(formatText(strAylikYuksek, 2));
                    aylik_degisim.setText(formatText(strAyllikDegisim, 2) + "%");
                    aylik_degisim.setTextColor(setColor(strAyllikDegisim));
                    yillik_degisim.setText(formatText(strYillikDegisim, 2) + "%");
                    yillik_degisim.setTextColor(setColor(strYillikDegisim));
                    yillik_dusuk.setText(formatText(strYillikDusuk, 2));
                    yillik_yuksek.setText(formatText(strYillikYuksek, 2));
                    code = strKod;
                    saat = strTime;
                    if (strGunlukDegisimOrani.contains("-")) {
                        son.setTextColor(getResources().getColor(R.color.textRedColor));
                        gunluk_degisim_orani.setTextColor(getResources().getColor(R.color.textRedColor));
                        gunluk_degisim.setTextColor(getResources().getColor(R.color.textRedColor));
                    } else {
                        son.setTextColor(getResources().getColor(R.color.textGreenColor));
                        gunluk_degisim_orani.setTextColor(getResources().getColor(R.color.textGreenColor));
                        gunluk_degisim.setTextColor(getResources().getColor(R.color.textGreenColor));
                    }
                    hisseDetayLoader.setVisibility(View.GONE);
                    hisseDetayContent.setVisibility(View.VISIBLE);
                    showAddButton();
                    if (isHisse) {
                        getDetail(strKod);
                    } else {
                        sirkeBilgiLoader.setVisibility(View.GONE);

                    }
                } catch (JSONException | InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int setColor(String val) {
        if (val != null && val.contains("-")) {
            return getResources().getColor(R.color.textRedColor);
        } else {
            return getResources().getColor(R.color.textGreenColor);
        }
    }

    private void showAddButton() {
        boolean varmi = helper.isAdded(c);
        ekle.setVisibility(View.VISIBLE);
        if (varmi) {
            ekle.setImageResource(R.drawable.btn_kaynak_ekle_active);
        } else {
            ekle.setImageResource(R.drawable.btn_kaynak_ekle);
        }
    }

    public String FormatCurrency(double cur) {
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("tr", "TR"));
        return nf.format(cur);

    }

    public void getDetail(String value) {
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setConnectTimeout(20 * 10000);
        client.setTimeout(20 * 10000);
        client.setResponseTimeout(20 * 10000);
        client.get("http://service.elluga.net/api/servis/kap/talep/kodbazlisirketozet/sirket/" + value + "/format/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody != null) {
                    try {
                        JSONObject obj = new JSONObject(new String(responseBody));
                        String strAdres = obj.getJSONArray("ozet_bilgileri").getJSONObject(0).getString("bilgi_degeri");
                        String strEposta = obj.getJSONArray("ozet_bilgileri").getJSONObject(1).getString("bilgi_degeri");
                        String strWeb = obj.getJSONArray("ozet_bilgileri").getJSONObject(2).getString("bilgi_degeri");
                        String strSektor = obj.getJSONArray("ozet_bilgileri").getJSONObject(6).getString("bilgi_degeri");
                        String strLogo = obj.getJSONObject("sirket_bilgileri").getString("sirket_logo");
                        Picasso.with(HisseDetay.this).load(strLogo).into(logo);
                        JSONArray arr = obj.getJSONArray("yetkili_bilgileri");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject o = arr.getJSONObject(i);
                            TableRow.LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            LinearLayout.LayoutParams viewParam = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                            View view = new View(HisseDetay.this);
                            view.setLayoutParams(viewParam);
                            view.setBackgroundColor(getResources().getColor(R.color.textLightGray));
                            MyTextView yetkiliTw = new MyTextView(HisseDetay.this);
                            MyTextView gorevTw = new MyTextView(HisseDetay.this);
                            yetkiliTw.setLayoutParams(params);
                            yetkiliTw.setText(o.getString("yetkili_adi"));
                            yetkiliTw.setCustomFont(HisseDetay.this, getString(R.string.UbuntuRegular));
                            yetkiliTw.setTextSize(15f);
                            yetkiliTw.setPadding(spToPixel(10), spToPixel(10), spToPixel(10), spToPixel(10));
                            gorevTw.setLayoutParams(params);
                            gorevTw.setText(o.getString("yetkili_gorevi"));
                            gorevTw.setTextSize(13f);
                            gorevTw.setCustomFont(HisseDetay.this, getString(R.string.UbuntuMedium));
                            gorevTw.setTextColor(getResources().getColor(R.color.black));
                            gorevTw.setPadding(spToPixel(10), 0, spToPixel(10), 0);

                            yetkililer.addView(yetkiliTw);
                            yetkililer.addView(gorevTw);
                            yetkililer.addView(view);
                        }

                        adres.setText(strAdres);
                        posta.setText(strEposta);
                        web.setText(strWeb);
                        sektor.setText(strSektor);
                        JSONArray array = obj.getJSONArray("son_bes_bildirim");
                        ArrayList<BildirimItems> list = new ArrayList<>();
                        for (int j = 0; j < array.length(); j++) {
                            JSONObject o = array.getJSONObject(j);
                            String kod = o.getString("sirket_kodu");
                            String unvan = o.getString("sirket_adi");
                            String title = o.getString("bildirim_baslik");
                            String date = o.getString("yayinlanma_tarihi_formatli");
                            String bildirimNo = o.getString("bildirim_numarasi");


                            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");

                            Log.d("date",date);
                            Log.d("df.parse", String.valueOf(df.parse(date)));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");

                            String saat = dateFormat.format(df.parse(date));


                            //String saat = reFormat("dd.MM.yyyy HH:mm","dd MMM EEE",date);
                            // String saat = df[1];
                            list.add(new BildirimItems(kod, saat, unvan, title, bildirimNo));
                        }
                        BildirimAdapter adapter = new BildirimAdapter(HisseDetay.this, R.layout.kap_fragment_list_item, list);
                        listItem.setAdapter(adapter);

                        sirkeBilgiLoader.setVisibility(View.GONE);
                        sirketBilgiContent.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public String getFormatedStringDate(String format,Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat (format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(date);
    }

    public String reFormat(String inFormat, String outFormat, String strDate){
        String resultDate = null;
        try {
            DateFormat format = new SimpleDateFormat(inFormat);
            Date date = format.parse(strDate);
            resultDate = getFormatedStringDate(outFormat, date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultDate;
    }

    public String formatText(String str, int s1) {
        if (str != null) {
            double t = Double.parseDouble(str);
            return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
        } else {
            return null;
        }
    }

    public String formatTime(String str) {
        char[] s = str.toCharArray();
        String t = ":";
        StringBuilder newString = new StringBuilder(s.length);
        for (int i = 0; i < s.length; i++) {
            if (i % 2 == 1) {
                if (i > 0) {
                    newString.append(s[i]).append(t);
                }
            } else {

                newString.append(s[i]);
            }
        }

        if (s.length > 0) {
            return newString.toString().substring(0, newString.length() - 1);

        } else {
            return str;
        }
    }

    public int spToPixel(int sp) {
        int scaledDensity = (int) getResources().getDisplayMetrics().scaledDensity;
        return sp * scaledDensity;
    }

    @SuppressLint("StaticFieldLeak")
    private class getChartData extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            HttpClient client = new HttpClient();
            String res = null;
            String url = "https://cloud.foreks.com/historical-service/intraday/code/" + strings[0] + "/period/1440/last/100";
            try {
                res = client.Get(url);
                System.out.println(url);
                //  res = client.Get ( "https://cloud.foreks.com/historical-service/intraday/code/"+c+"/period/1440/last/30" );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                fillChart(s);
                chartLoaded = true;
                resource = s;
            }
        }
    }

    private void fillChart(String s) {
        chart.setOnChartGestureListener(this);
        chart.setOnChartValueSelectedListener(this);
        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        chart.setTouchEnabled(true);
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setPinchZoom(true);
        chart.getAxisRight().setEnabled(false);
        chart.invalidate();
        Legend l = chart.getLegend();
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.LINE);
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(Typeface.createFromAsset(getAssets(), "font/Ubuntu-Regular.ttf"));
        xAxis.setTextSize(8f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(true);
        xAxis.setCenterAxisLabels(true);

        xAxis.setValueFormatter(new IAxisValueFormatter() {

            private SimpleDateFormat mFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                long tr = (long) value;
                return mFormat.format(value);
            }
        });


        ArrayList<HistoryItems> list = new ArrayList<>();
        JSONArray arr;
        try {
            arr = new JSONArray(s);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject object = arr.getJSONObject(i);
                list.add(new HistoryItems((object.has("d")) ? object.getString("d") : null, (object.has("o")) ? object.getString("o") : null, (object.has("h")) ? object.getString("h") : null, (object.has("l")) ? object.getString("l") : null, (object.has("c")) ? object.getString("c") : null, (object.has("v")) ? object.getString("v") : null, (object.has("a")) ? object.getString("a") : null));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.enableGridDashedLine(5, 5, 0);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(true);
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                double d = (double) value;
                return String.format(Locale.getDefault(), "%.3f", d);
            }
        });
        leftAxis.setSpaceTop(50);
        leftAxis.setSpaceBottom(15);
        chart.getXAxis().setGranularity(3600 * 24 * 30);
        ArrayList<Entry> arrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            HistoryItems item = list.get(i);

            long tr = Long.parseLong(item.getD()==null?"0":item.getD());
            float val = Float.parseFloat(item.getC()==null?"0":item.getC());
            arrayList.add(new Entry(tr, val));


        }

        if (arrayList.size() > 0) {
            LineDataSet set1 = new LineDataSet(arrayList, code);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setColor(getResources().getColor(R.color.textLightGray));
            set1.setCircleColor(getResources().getColor(R.color.black));
            set1.setValueTextColor(ColorTemplate.getHoloBlue());
            set1.setLineWidth(.5f);
            set1.setHighlightLineWidth(1.f);
            set1.setCircleRadius(1);
            set1.setDrawCircleHole(false);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setValueTypeface(Typeface.createFromAsset(this.getAssets(), "font/Ubuntu-Regular.ttf"));
            set1.setFormLineWidth(3f);
            set1.setFormSize(15f);
            set1.setFillAlpha(70);
            set1.setFillColor(Color.parseColor("#FF229FC2"));
            set1.setDrawCircles(false);
            set1.setHighLightColor(getResources().getColor(R.color.textRedColor));
            LineData data = new LineData(set1);
            chart.setData(data);
            chart.notifyDataSetChanged();
            chart.getData().notifyDataChanged();
            MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker);
            chart.setMarker(mv);
            noData = false;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getUsdLast extends AsyncTask<String, String, Double> {

        @Override
        protected Double doInBackground(String... strings) {
            double last = 0;
            try {
                String res = new HttpClient().Get(getResources().getString(R.string.usdTRYLastApi));
                if (res != null) {
                    JSONArray arr = new JSONArray(res);
                    last = arr.getJSONObject(0).getDouble("last");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return last;
        }
    }

    public String clearCode(String sembol) {
        return sembol.replace(".OANA.VIOP", "")
                .replace(".FANA.VIOP", "")
                .replace(".DFAN.VIOP", "")
                .replace(".EFAN.VIOP", "")
                .replace(".KFAN.VIOP", "")
                .replace(".EVAN.VIOP", "")
                .replace(".BFAN.VIOP", "")
                .replace(".ORAN.VIOP", "")
                .replace(".CFAN.VIOP", "")
                .replace(".MFAN.VIOP", "")
                .replace(".YEFA.VIOP", "")
                .replace(".DOAN.VIOP", "")
                .replace(".EOAN.VIOP", "");
    }
}

