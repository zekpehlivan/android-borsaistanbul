package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.activities.HisseDetay;
import com.borsaistanbul.mobil.activities.MainActivity;
import com.borsaistanbul.mobil.adapters.HisselerAdapter;
import com.borsaistanbul.mobil.adapters.HisselerItems;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class Hisseler extends AppCompatActivity implements View.OnClickListener, TextWatcher, AdapterView.OnItemClickListener {
    MyTextView date;
    ListView listItem;
    Button artan;
    Button azalan;
    Button bist;
    ArrayList <HisselerItems> list = new ArrayList <> ( );
    ArrayList <HisselerItems> filteredlist = new ArrayList <> ( );
    HisselerAdapter adapter;
    HisselerAdapter adapter1;
    EditText query;
    int type = 1;
    Handler handler = new Handler ( );
    Timer timer = new Timer ( );
    Runnable runnable;
    boolean needLoader = false;
    private FirebaseAnalytics analytics;
    String index;
    String desc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_hisseler);
        index = getIntent().getStringExtra("c");
        desc = getIntent().getStringExtra("desc");
        ActionBar tb = getSupportActionBar();
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled(true);
        tb.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.navColor)));
        //  tb.setTitle(unvan);
        tb.setDisplayShowCustomEnabled(true);
        tb.setCustomView(R.layout.custom_title_bar);

        TextView text = findViewById(R.id.action_bar_title);
        text.setText(desc);
        date = findViewById(R.id.date);
        Calendar c = Calendar.getInstance ( );
        Date d = c.getTime ( );
        SimpleDateFormat df = new SimpleDateFormat ( "dd MMMM EEEE", Locale.getDefault ( ) );
        String dateOfDay = df.format ( d );
        date.setText ( dateOfDay );
        listItem = findViewById(R.id.listItem);
        listItem.setOnItemClickListener ( this );
        azalan = findViewById(R.id.azalan);
        artan = findViewById(R.id.artan);
        bist = findViewById(R.id.bist);
        azalan.setOnClickListener ( this );
        artan.setOnClickListener ( this );
        bist.setOnClickListener ( this );
        query = findViewById(R.id.query);
        query.addTextChangedListener ( this );
        adapter1 = new HisselerAdapter(this, R.layout.hisse_list_item, filteredlist);
        adapter = new HisselerAdapter(this, R.layout.hisse_list_item, list);
        listItem.setAdapter ( adapter );

        bist.performClick ( );
        runnable = new Runnable ( ) {
            @Override
            public void run ( ) {
                new getHisselist ( ).execute ( "" );
            }
        };
        timer.schedule ( new TimerTask ( ) {
            @Override
            public void run ( ) {
                handler.post ( runnable );
            }
        }, 100, 10000 );


    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onClick ( View view ) {

        if ( view == bist ) {
            bist.setSelected ( true );
            artan.setSelected ( false );
            azalan.setSelected ( false );
            type = 1;
            setAdapter ( );
            filters ( );
            sortList ( );

        }
        if ( view == artan ) {
            bist.setSelected ( false );
            artan.setSelected ( true );
            azalan.setSelected ( false );
            type = 2;
            setAdapter ( );
            filters ( );
            sortList ( );

        }
        if ( view == azalan ) {
            bist.setSelected ( false );
            artan.setSelected ( false );
            azalan.setSelected ( true );
            type = 3;
            setAdapter ( );
            filters ( );
            sortList ( );


        }
    }

    private void setAdapter ( ) {
        if ( type == 1 ) {
            if ( !query.getText ( ).toString ( ).equals ( "" ) ) {
                listItem.setAdapter ( adapter1 );

            } else {
                listItem.setAdapter ( adapter );

            }
        } else {
            listItem.setAdapter ( adapter1 );
        }
    }


    @Override
    public void beforeTextChanged ( CharSequence charSequence, int i, int i1, int i2 ) {
        listItem.setAdapter ( adapter1 );
    }

    @Override
    public void onTextChanged ( CharSequence charSequence, int i, int i1, int i2 ) {

    }

    @Override
    public void afterTextChanged ( Editable editable ) {
        setAdapter ( );
        filters ( );

    }

    private void filters ( ) {
        String text = query.getText ( ).toString ( ).toUpperCase ( ).trim ( )
                .replace("İ", "I")
                .replace("Ş", "S")
                .replace("Ü", "U")
                .replace("Ö", "O")
                .replace("Ç", "C")
                .replace("Ğ", "G")
                .replace("i", "I")
                .replace("ş", "S")
                .replace("ç", "C")
                .replace("ö", "O")
                .replace("ü", "U")
                .replace("ğ", "G");

        filteredlist.clear ( );
        for ( int i = 0 ; i < list.size ( ) ; i++ ) {
            if ( type == 1 ) {
                if ( !text.equals ( "" ) ) {
                    if ( list.get ( i ).getSembol ( ).contains ( text ) ) {
                        filteredlist.add ( list.get ( i ) );
                    } else {
                        String item = list.get ( i ).getName ( )
                                .replace("İ", "I")
                                .replace("Ş", "S")
                                .replace("Ü", "U")
                                .replace("Ö", "O")
                                .replace("Ç", "C")
                                .replace("Ğ", "G")
                                .replace("i", "I")
                                .replace("ş", "S")
                                .replace("ç", "C")
                                .replace("ö", "O")
                                .replace("ü", "U")
                                .replace("ğ", "G");

                        if ( item.contains ( text ) ) {
                            filteredlist.add ( list.get ( i ) );
                        }
                    }
                }

            }
            if ( type == 2 ) {
                if ( !text.equals ( "" ) ) {
                    if ( list.get ( i ).getSembol ( ).contains ( text ) ) {
                        if ( !list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                            filteredlist.add ( list.get ( i ) );
                        }
                    } else {
                        if ( !list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                            String item = list.get ( i ).getName ( )
                                    .replace("İ", "I")
                                    .replace("Ş", "S")
                                    .replace("Ü", "U")
                                    .replace("Ö", "O")
                                    .replace("Ç", "C")
                                    .replace("Ğ", "G")
                                    .replace("i", "I")
                                    .replace("ş", "S")
                                    .replace("ç", "C")
                                    .replace("ö", "O")
                                    .replace("ü", "U")
                                    .replace("ğ", "G");

                            if ( item.contains ( text ) ) {
                                filteredlist.add ( list.get ( i ) );
                            }
                        }
                    }
                } else {
                    if ( !list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                        filteredlist.add ( list.get ( i ) );
                    }
                }

            }
            if ( type == 3 ) {
                if ( !text.equals ( "" ) ) {
                    if ( list.get ( i ).getSembol ( ).contains ( text ) ) {


                        if ( list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                            filteredlist.add ( list.get ( i ) );
                        }
                    } else {
                        if ( list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                            String item = list.get ( i ).getName ( )
                                    .replace("İ", "I")
                                    .replace("Ş", "S")
                                    .replace("Ü", "U")
                                    .replace("Ö", "O")
                                    .replace("Ç", "C")
                                    .replace("Ğ", "G")
                                    .replace("i", "I")
                                    .replace("ş", "S")
                                    .replace("ç", "C")
                                    .replace("ö", "O")
                                    .replace("ü", "U")
                                    .replace("ğ", "G");

                            if ( item.contains ( text ) ) {
                                filteredlist.add ( list.get ( i ) );
                            }
                        }
                    }
                } else {
                    if ( list.get ( i ).getDegisim ( ).contains ( "-" ) ) {
                        filteredlist.add ( list.get ( i ) );
                    }
                }


            }

        }

        selectAdapter ( );

    }

    private void selectAdapter ( ) {
        if ( !query.getText ( ).toString ( ).equals ( "" ) ) {
            adapter1.notifyDataSetChanged ( );
        } else {
            if ( type > 1 ) {
                adapter1.notifyDataSetChanged ( );
            } else {
                adapter.notifyDataSetChanged ( );
            }
        }
    }

    @Override
    public void onItemClick ( AdapterView <?> adapterView, View view, int i, long l ) {
        HisselerItems items = (HisselerItems) adapterView.getItemAtPosition ( i );
        startActivity(new Intent(this, HisseDetay.class).putExtra("c", items.getC()).putExtra("hisse", "e"));
    }


    @SuppressLint ( "StaticFieldLeak" )
    private class getHisselist extends AsyncTask <Object, Integer, String> {
        LoaderDialog dialog;
        String result;

        @Override
        protected void onPreExecute ( ) {
            dialog = new LoaderDialog(Hisseler.this);
            if ( !needLoader ) {
                dialog.show ( );
            }
        }

        @Override
        protected String doInBackground ( Object... objects ) {
            HttpClient client = new HttpClient ( );
            try {
                if (index.isEmpty()) {
                    result = client.Get(getResources().getString(R.string.searchApi) + "&security=E");

                } else {
                    result = client.Get(getResources().getString(R.string.forexHisselerApi) + "&index=" + index);

                }
            } catch ( Exception e ) {
                e.printStackTrace ( );
            }
            return null;
        }

        @Override
        protected void onPostExecute ( String s ) {
            if ( result != null ) {
                if ( list.size ( ) > 0 ) {
                    list.clear ( );
                }
                try {
                    JSONArray jsonArray = new JSONArray ( result );
                    for ( int i = 0 ; i < jsonArray.length ( ) ; i++ ) {
                        JSONObject object = jsonArray.getJSONObject ( i );
                        String degisim = formatText(object.getString("dailyChangePercentage"), 2);
                        String sembol = object.getString ( "legacyCode" );
                        String saat = object.getString ( "time" );
                        String son = object.getString ( "last" );
                        String name = object.getString ( "description" );
                        String c = object.getString ( "c" );
                        String yon = object.getString ( "dailyChangePercentageDirection" );
                        long volume = object.getLong("dailyVolume");
                        list.add(new HisselerItems(sembol, degisim, son, saat, name, c, yon, volume));
                    }
                    sortList ( );

                    adapter.notifyDataSetChanged ( );

                } catch ( JSONException e ) {
                    e.printStackTrace ( );
                }
            }
            if ((dialog != null) && dialog.isShowing()) {
                dialog.dismiss ( );
                needLoader = true;
            }

            filters ( );

        }
    }

    private void sortList ( ) {
        if ( type == 1 ) {
            Collections.sort ( list, new Comparator <HisselerItems> ( ) {
                @Override
                public int compare ( HisselerItems hisselerItems, HisselerItems t1 ) {
                    Long s1 = hisselerItems.getDailyVolume();
                    Long s2 = t1.getDailyVolume();
                    return Long.compare(s2, s1);
                }


            } );
            filters ( );
        }
        if ( type == 2 ) {

            Collections.sort ( list, new Comparator <HisselerItems> ( ) {
                @Override
                public int compare ( HisselerItems hisselerItems, HisselerItems t1 ) {

                    NumberFormat format = NumberFormat.getInstance(new Locale("tr","TR"));

                    double s1 = 0;
                    double s2 =0;
                    try {
                        s1 = (double) format.parse ( hisselerItems.getDegisim ( ) ).doubleValue();
                        s2 = (double) format.parse ( t1.getDegisim ( )  ).doubleValue();

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return Double.compare ( s2, s1 );
                }
            } );
            filters ( );
        }
        if ( type == 3 ) {
            Collections.sort ( list, new Comparator <HisselerItems> ( ) {
                @Override
                public int compare ( HisselerItems hisselerItems, HisselerItems t1 ) {
                    NumberFormat format = NumberFormat.getInstance(new Locale("tr","TR"));

                    double s1 = 0;
                    double s2 =0;
                    try {
                        s1 = (double) format.parse ( hisselerItems.getDegisim ( ) ).doubleValue();
                        s2 = (double) format.parse ( t1.getDegisim ( )  ).doubleValue();

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    return Double.compare ( s1, s2 );
                }
            } );
            filters ( );
        }
    }
    public String formatText(String str, int s1) {
        double t = Double.parseDouble(str);
        return String.format(Locale.getDefault(), "%,." + s1 + "f", t);
    }
    @Override
    public void onDestroy ( ) {
        super.onDestroy ( );
        timer.cancel ( );
    }
}
