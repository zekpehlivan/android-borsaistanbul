package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.borsaistanbul.mobil.BaseActivity;
import com.borsaistanbul.mobil.R;

public class Browser extends BaseActivity {
    ImageView resim;
    WebView content;

    @SuppressLint ( "SetJavaScriptEnabled" )
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_browser );
        ActionBar tb = getSupportActionBar ( );
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled ( true );
        tb.setBackgroundDrawable ( new ColorDrawable ( getResources ( ).getColor ( R.color.navColor ) ) );
        tb.setTitle ( "" );
        Intent i = getIntent ( );
        String bildirimNo = i.getStringExtra ( "bid" );

        content = findViewById ( R.id.browser );
        content.setWebViewClient ( new WebViewClient ( ) );
        content.getSettings ( ).setJavaScriptEnabled ( true );
        content.loadUrl ( "https://www.kap.org.tr/tr/Bildirim/" + bildirimNo );

    }

    @Override
    public boolean onSupportNavigateUp ( ) {
        finish ( );
        return true;
    }

    @Override
    public void onBackPressed ( ) {
        finish ( );
    }
}
