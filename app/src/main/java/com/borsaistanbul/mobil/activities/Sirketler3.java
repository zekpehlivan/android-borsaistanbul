package com.borsaistanbul.mobil.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.adapters.SirketlerAdapter;
import com.borsaistanbul.mobil.adapters.SirketlerItems;
import com.borsaistanbul.mobil.helper.HttpClient;
import com.borsaistanbul.mobil.helper.LoaderDialog;
import com.borsaistanbul.mobil.helper.MyEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Sirketler3 extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, TextWatcher {
    ListView listItem;
    View row;
    MyEditText search;
    ArrayList<SirketlerItems> list = new ArrayList<>();
    ArrayList<SirketlerItems> filteredList = new ArrayList<>();
    SirketlerAdapter adapter;
    SirketlerAdapter adapter1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sirketler);
        ActionBar tb = getSupportActionBar();
        assert tb != null;
        tb.setDisplayHomeAsUpEnabled(true);
        tb.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.navColor)));
        //  tb.setTitle(unvan);
        tb.setDisplayShowCustomEnabled(true);
        tb.setCustomView(R.layout.custom_title_bar);

        TextView text = findViewById(R.id.action_bar_title);
        text.setText("Bist Şirketler");
        listItem = findViewById(R.id.listItem);
        listItem.setOnItemClickListener(this);
        search = findViewById(R.id.query);
        search.addTextChangedListener(this);
        adapter1 = new SirketlerAdapter(this, R.layout.sirketler_items, filteredList);
        adapter = new SirketlerAdapter(this, R.layout.sirketler_items, list);
        new getSirketList().execute("");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        SirketlerItems items = (SirketlerItems) adapterView.getItemAtPosition(i);
        startActivity(new Intent(this, SirketDetay.class).putExtra("c", items.getLink()).putExtra("u", items.getUnvan()));
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    private class getSirketList extends AsyncTask<Object, String, String> {
        String result;
        LoaderDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new LoaderDialog(Sirketler3.this);
            dialog.show();
        }

        @Override
        protected String doInBackground(Object... objects) {
            HttpClient client = new HttpClient();
            try {
                result = client.Get("http://service.elluga.net/api/servis/kap/talep/bistsirketliste/format/json");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                try {
                    JSONArray arr = new JSONArray(s);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj = arr.getJSONObject(i);
                        String kod = obj.getString("sirket_kodu");
                        String unvan = obj.getString("sirket_unvani");
                        String grup = obj.getString("sirket_grubu");
                        String sehir = obj.getString("sirket_sehri");
                        String denetmen = obj.getString("denetim_kurulusu");
                        String link = obj.getString("ozet_linki");
                        list.add(new SirketlerItems(kod, unvan, grup, sehir, denetmen, link));
                    }

                    listItem.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            dialog.dismiss();

        }


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String text = search.getText().toString().toUpperCase().trim()
                .replace("İ", "I")
                .replace("Ş", "S")
                .replace("Ü", "U")
                .replace("Ö", "O")
                .replace("Ç", "C")
                .replace("Ğ", "G");
        String txtUnvan = search.getText().toString().toUpperCase().trim();
        if (filteredList.size() > 0) {
            filteredList.clear();
        }
        if (text.length() > 0) {
            //  filteredList.clear();
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getKod().contains(text)) {

                    filteredList.add(list.get(j));
                } else {
                    String item = list.get(j).getUnvan()
                            .replace("İ", "I")
                            .replace("Ş", "S")
                            .replace("Ü", "U")
                            .replace("Ö", "O")
                            .replace("Ç", "C")
                            .replace("Ğ", "G");

                    if (item.contains(text)) {
                        filteredList.add(list.get(j));
                    }
                }
            }
            listItem.setAdapter(adapter1);

        } else {

            listItem.setAdapter(adapter);

        }
    }

}
