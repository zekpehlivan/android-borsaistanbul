package com.borsaistanbul.mobil.adapters;

@SuppressWarnings("WeakerAccess")
public class SirketlerItems {
    String kod, unvan, grup, sehir, denetmen, link;

    public SirketlerItems(String kod, String unvan, String grup, String sehir, String denetmen, String link) {
        this.kod = kod;
        this.unvan = unvan;
        this.grup = grup;
        this.sehir = sehir;
        this.denetmen = denetmen;
        this.link = link;
    }

    public String getKod ( ) {
        return kod;
    }

    public String getUnvan ( ) {
        return unvan;
    }

    public String getGrup ( ) {
        return grup;
    }

    public String getSehir ( ) {
        return sehir;
    }

    public String getDenetmen ( ) {
        return denetmen;
    }

    public String getLink ( ) {
        return link;
    }
}
