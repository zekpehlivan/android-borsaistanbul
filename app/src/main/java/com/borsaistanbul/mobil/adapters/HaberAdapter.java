package com.borsaistanbul.mobil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class HaberAdapter extends ArrayAdapter<HaberItems> {
    private Context ctx;
    private int res;
    private ArrayList<HaberItems> list;

    public HaberAdapter(@NonNull Context context, int resource, ArrayList<HaberItems> items) {
        super(context, resource, items);
        this.ctx = context;
        this.res = resource;
        this.list = items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();
            holder.img = row.findViewById(R.id.img);
            holder.title = row.findViewById(R.id.title);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        HaberItems items = list.get(position);
        Picasso.with(ctx).load(items.getImg()).into(holder.img);
        holder.title.setText(items.getTitle());
        return row;
    }

    static class ViewHolder {
        ImageView img;
        MyTextView title;
    }
}
