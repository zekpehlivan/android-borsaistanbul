package com.borsaistanbul.mobil.adapters;

import java.util.Locale;

public class AnasayfaItems {
    private String sembol, degisim, son, saat, name, c, yon, gunlukDegisim;
    private long hacim;

    public AnasayfaItems(String sembol, String degisim, String son, String saat, String name, String c, String yon, String gunlukDegisim, Long hacim) {
        this.sembol = sembol;
        this.degisim = degisim;
        this.son = son;
        this.saat = saat;
        this.name = name;
        this.c = c;
        this.yon = yon;
        this.gunlukDegisim = gunlukDegisim;
        this.hacim = hacim;

    }

    public String getSembol() {
        return sembol.replace(".OANA.VIOP", "")
                .replace(".FANA.VIOP", "")
                .replace(".DFAN.VIOP", "")
                .replace(".EFAN.VIOP", "")
                .replace(".KFAN.VIOP", "")
                .replace(".EVAN.VIOP", "")
                .replace(".BFAN.VIOP", "")
                .replace(".ORAN.VIOP", "")
                .replace(".CFAN.VIOP", "")
                .replace(".MFAN.VIOP", "")
                .replace(".YEFA.VIOP", "")
                .replace(".DOAN.VIOP", "")
                .replace(".EOAN.VIOP", "");
    }

    public String getGunlukDegisim() {
        return gunlukDegisim;
    }

    public String getDegisim() {
        return degisim;
    }

    public String getSon() {
        return son;
    }

    public String getSaat() {
        if (saat.length() > 0) {
            char[] s = saat.toCharArray();
            String t = ":";
            StringBuilder newString = new StringBuilder(s.length);
            for (int i = 0; i < s.length; i++) {
                if (i % 2 == 1) {
                    if (i > 0) {
                        newString.append(s[i]).append(t);
                    }
                } else {

                    newString.append(s[i]);
                }
            }


            return newString.toString().substring(0, newString.length() - 1);
        } else {
            return saat;
        }

    }

    public String getYon() {
        return yon;
    }

    public String getName() {
        return name;
    }

    public String getC() {
        return c;
    }

    public long getHacim() {
        return hacim;
    }
}


