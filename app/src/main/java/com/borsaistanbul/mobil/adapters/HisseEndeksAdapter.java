package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;


public class HisseEndeksAdapter extends ArrayAdapter<HisseEndeksListItem> {
    private Context ctx;
    private int res;
    private ArrayList<HisseEndeksListItem> list;
    private View left;
    private View rght;
    private SharedPreferences pref;

    public HisseEndeksAdapter(Context ctx, int res, ArrayList<HisseEndeksListItem> list) {
        super(ctx, res, list);
        this.ctx = ctx;
        this.res = res;
        this.list = list;

    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();
            holder.sembol = row.findViewById(R.id.sembol);
            row.setTag(holder);
        } else {

            holder = (ViewHolder) row.getTag();

        }

        final HisseEndeksListItem items = list.get(position);

        holder.sembol.setText(items.getDesc());
        holder.sembol.setTextColor(ctx.getResources().getColor(R.color.black));


        return row;
    }

    static class ViewHolder {
        MyTextView sembol;


    }

}
