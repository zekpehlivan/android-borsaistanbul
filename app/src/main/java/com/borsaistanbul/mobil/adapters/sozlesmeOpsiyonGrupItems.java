package com.borsaistanbul.mobil.adapters;

/**
 * Created by duran on 16.11.2017.
 */

public class sozlesmeOpsiyonGrupItems {
    private String ad, filtre;

    public sozlesmeOpsiyonGrupItems(String ad, String filtre) {
        this.ad = ad;
        this.filtre = filtre;
    }

    public String getAd() {
        return ad;
    }

    public String getFiltre() {
        return filtre;
    }
}
