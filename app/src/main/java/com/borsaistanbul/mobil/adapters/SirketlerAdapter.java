package com.borsaistanbul.mobil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;

@SuppressWarnings("WeakerAccess")
public class SirketlerAdapter extends ArrayAdapter<SirketlerItems> {
    int resource;
    Context context;
    ArrayList<SirketlerItems> list;

    public SirketlerAdapter(@NonNull Context context, int resource, ArrayList<SirketlerItems> list) {
        super(context, resource, list);
        this.resource = resource;
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if ( row == null ) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
            assert layoutInflater != null;
            row = layoutInflater.inflate ( resource, parent, false );
            holder = new ViewHolder ( );
            holder.kod = row.findViewById ( R.id.kod );
            holder.denetmen = row.findViewById ( R.id.denetmen );
            holder.sehir = row.findViewById ( R.id.sehir );
            holder.grup = row.findViewById ( R.id.grup );
            holder.unvan = row.findViewById ( R.id.unvan );
            row.setTag ( holder );
        } else {
            holder = (ViewHolder) row.getTag ( );
        }
        SirketlerItems items = list.get ( position );
        String kod = items.getKod ( );
        String unvan = items.getUnvan ( );
        String sehir = items.getSehir ( );
        String grup = items.getGrup ( );
        String denetmen = items.getDenetmen ( );
        holder.unvan.setText ( unvan );
        holder.kod.setText ( kod );
        holder.sehir.setText ( sehir );
        holder.grup.setText ( grup );
        holder.denetmen.setText ( denetmen );
        return row;
    }

    static class ViewHolder {
        MyTextView kod, grup, sehir, unvan, denetmen;
    }
}
