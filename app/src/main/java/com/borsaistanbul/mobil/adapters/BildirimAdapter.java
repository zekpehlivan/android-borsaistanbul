package com.borsaistanbul.mobil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;

public class BildirimAdapter extends ArrayAdapter <BildirimItems> {
    private int res;
    private Context ctx;
    private ArrayList <BildirimItems> list;

    public BildirimAdapter ( Context ctx, int res, ArrayList <BildirimItems> list ) {
        super ( ctx, res, list );
        this.res = res;
        this.ctx = ctx;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView ( int position, @Nullable View convertView, @NonNull ViewGroup parent ) {
        View row = convertView;
        ViewHolder holder;
        if ( row == null ) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
            assert layoutInflater != null;
            row = layoutInflater.inflate ( res, parent, false );
            holder = new ViewHolder ( );
            holder.kod = row.findViewById ( R.id.kod );
            holder.saat = row.findViewById ( R.id.saat );
            holder.unvan = row.findViewById ( R.id.unvan );
            holder.title = row.findViewById ( R.id.title );
            row.setTag ( holder );
        } else {
            holder = (ViewHolder) row.getTag ( );
        }
        BildirimItems items = list.get ( position );
        String title = items.getTitle ( );
        String unvan = items.getUnvan ( );
        String saat = items.getSaat ( );
        String kod = items.getKod ( );
        if ( kod.equals ( "" ) ) {
            kod = "-";
        }

        holder.title.setText ( title );
        holder.unvan.setText ( unvan );
        holder.saat.setText ( saat );
        holder.kod.setText ( kod );
        return row;
    }

    static class ViewHolder {
        MyTextView kod;
        MyTextView saat;
        MyTextView unvan;
        MyTextView title;
    }
}
