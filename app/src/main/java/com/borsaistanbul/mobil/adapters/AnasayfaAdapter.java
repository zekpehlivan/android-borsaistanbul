package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;


public class AnasayfaAdapter extends ArrayAdapter<AnasayfaItems> {
    private Context ctx;
    private int res;
    private ArrayList<AnasayfaItems> list;
    private View left;
    private View rght;
    private SharedPreferences pref;

    public AnasayfaAdapter(Context ctx, int res, ArrayList<AnasayfaItems> list) {
        super(ctx, res, list);
        this.ctx = ctx;
        this.res = res;
        this.list = list;
        pref = ctx.getSharedPreferences("borsa", Context.MODE_PRIVATE);

    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();


            holder.degisim = row.findViewById(R.id.gunlukDegisim);
            holder.saat = row.findViewById(R.id.saat);
            holder.sembol = row.findViewById(R.id.sembol);
            holder.son = row.findViewById(R.id.son);
            holder.degisimOran = row.findViewById(R.id.degisimOran);


            row.setTag(holder);
        } else {

            holder = (ViewHolder) row.getTag();

        }

        final AnasayfaItems items = list.get(position);


        left = row.findViewById(R.id.hisse_left);
        rght = row.findViewById(R.id.hisse_right);


        holder.sembol.setText(items.getSembol());
        holder.degisimOran.setText("(%" + items.getDegisim() + ")");
        holder.degisim.setText(items.getGunlukDegisim());
        holder.saat.setText(items.getSaat());
        holder.son.setText(items.getSon());

        if (items.getDegisim().contains("-")) {
            holder.son.setTextColor(ctx.getResources().getColor(R.color.textRedColor));
            holder.degisim.setTextColor(ctx.getResources().getColor(R.color.textRedColor));
            holder.degisimOran.setTextColor(ctx.getResources().getColor(R.color.textRedColor));

        } else {

            holder.son.setTextColor(ctx.getResources().getColor(R.color.textGreenColor));
            holder.degisim.setTextColor(ctx.getResources().getColor(R.color.textGreenColor));
            holder.degisimOran.setTextColor(ctx.getResources().getColor(R.color.textGreenColor));
        }

        String time = pref.getString(items.getC(), "");
        boolean ref = time.equals(items.getSaat());
        tintBackground(items.getYon());
        if (!ref) {
            String color = items.getYon();
            tintBackground(color);
            // pref.edit().putString(items.getC(), items.getSaat()).apply();

        } else {

            left.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_left_bg));
            rght.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_rihgt_bg));
        }


        return row;
    }

    static class ViewHolder {
        MyTextView sembol;
        MyTextView degisim;
        MyTextView son;
        MyTextView saat;
        MyTextView degisimOran;
    }


    private void tintBackground(String color) {
        int c = (int) Double.parseDouble(color);
        Log.i("Renk", "" + c);
        Resources res = ctx.getResources();
        Drawable[] drawables;
        Drawable[] drawable2;
        if (c == 1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_green), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_green), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else if (c == -1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_red), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_red), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_gray), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_gray), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        }


        TransitionDrawable trans = new TransitionDrawable(drawables);
        TransitionDrawable trans2 = new TransitionDrawable(drawable2);
        left.setBackground(trans);
        rght.setBackground(trans2);
        trans.startTransition(500);
        trans2.startTransition(500);
    }

    private String aciklamaDuzelt(String legacyCode) {

        switch (legacyCode) {
            case "SUSD":
                legacyCode = "USD";
                break;
            case "SEUR":
                legacyCode = "EUR";
                break;
            case "SSAR":
                legacyCode = "SAR";
                break;
            case "SCAD":
                legacyCode = "CAD";
                break;
            case "SCHF":
                legacyCode = "CHF";
                break;
            case "SGBP":
                legacyCode = "GBP";
                break;
            case "SJPY":
                legacyCode = "JPY";
                break;
            case "XGLD":
                legacyCode = "Spot Altın";
                break;
            case "SGCEYREK":
                legacyCode = "Çeyrek Altın";
                break;
            case "SGYARIM":
                legacyCode = "Yarım Altın";
                break;
            case "SCUM":
                legacyCode = "Cumhuriyet Altını";
                break;
            case "SGATA":
                legacyCode = "Ata Lira";
                break;
            case "SGBESLI":
                legacyCode = "Beşli Altın";
                break;
            case "SGIKIBUCUK":
                legacyCode = "İkibuçuk Altın";
                break;
            case "SGGREMSE":
                legacyCode = "Gremse Altın";
                break;
            case "SG14BIL":
                legacyCode = "14 Ayar Bilezik";
                break;
            case "SG18BIL":
                legacyCode = "18 Ayar Bilezik";
                break;
            case "SG22BIL":
                legacyCode = "22 Ayar Bilezik";
                break;
            case "SGZIYNET":
                legacyCode = "Ziynet Altın";
                break;
            case "EURGLD":
                legacyCode = "Külçe Altın (EUR)";
                break;
            case "USGLD":
                legacyCode = "Külçe Altın (USD)";
                break;
            case "USGLDKG":
                legacyCode = "Külçe Altın (KG)";
                break;
            case "SGLD":
                legacyCode = "Gram Altın";
                break;
            case "XHGLD":
                legacyCode = "Has Altın";
                break;
            case "XSLV":
                legacyCode = "Gram Gümüş";
                break;
            case "XU100":
                legacyCode = "BIST 100";
                break;
            case "XU050":
                legacyCode = "BIST 50";
                break;
            case "XU030":
                legacyCode = "BIST 30";
                break;


        }
        return legacyCode;
    }
}
