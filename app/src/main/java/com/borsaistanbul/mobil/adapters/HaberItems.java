package com.borsaistanbul.mobil.adapters;


@SuppressWarnings("WeakerAccess")
public class HaberItems {
    private String id, img, text, title;

    public HaberItems(String id, String img, String text, String title) {
        this.id = id;
        this.img = img;
        this.text = text;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getImg() {
        return img;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }
}
