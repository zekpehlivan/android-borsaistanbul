package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;


public class HisselerAdapter extends ArrayAdapter <HisselerItems> {
    private Context ctx;
    private int res;
    private ArrayList <HisselerItems> list;
    private DbHelper helper;
    private View left, rght;
    private SharedPreferences pref;
    LayoutInflater layoutInflater;

    public HisselerAdapter ( @NonNull Context context, int resource, ArrayList <HisselerItems> hisselerItems ) {
        super ( context, resource, hisselerItems );
        this.ctx = context;
        this.res = resource;
        this.list = hisselerItems;
        pref = ctx.getSharedPreferences("borsa", Context.MODE_PRIVATE);
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @SuppressLint ( "SetTextI18n" )
    @NonNull
    @Override
    public View getView ( int position, @Nullable View convertView, @NonNull ViewGroup parent ) {
        View row = convertView;
        final ViewHolder holder;

        if ( row == null ) {
            row = layoutInflater.inflate ( res, parent, false );
            holder = new ViewHolder ( );
            holder.icon = row.findViewById ( R.id.btnEkle );

            if ( holder.icon == null ) {
                holder.degisim = row.findViewById ( R.id.degisim );
                holder.saat = row.findViewById ( R.id.saat );
                holder.sembol = row.findViewById ( R.id.sembol );
                holder.son = row.findViewById ( R.id.son );
            } else {
                holder.sembol = row.findViewById ( R.id.sembol );
                holder.name = row.findViewById ( R.id.name );
            }

            row.setTag ( holder );
        } else {

            holder = (ViewHolder) row.getTag ( );

        }

        final HisselerItems items = list.get ( position );
        helper = new DbHelper ( ctx );
        left = row.findViewById ( R.id.hisse_left );
        rght = row.findViewById ( R.id.hisse_right );
        if ( holder.icon == null ) {

            holder.sembol.setText ( items.getSembol ( ) );
            holder.degisim.setText("(%" + items.getDegisim() + ")");
            holder.saat.setText ( items.getSaat ( ) );
            holder.son.setText ( items.getSon ( ) );

            if ( items.getDegisim ( ).contains ( "-" ) ) {

                holder.son.setTextColor ( ctx.getResources ( ).getColor ( R.color.textRedColor ) );
                holder.degisim.setTextColor ( ctx.getResources ( ).getColor ( R.color.textRedColor ) );

            } else {

                holder.son.setTextColor ( ctx.getResources ( ).getColor ( R.color.textGreenColor ) );
                holder.degisim.setTextColor ( ctx.getResources ( ).getColor ( R.color.textGreenColor ) );

            }

            String time = pref.getString(items.getC(), "");
            boolean ref = time.equals ( items.getSaat ( ) );

            if (!ref) {
                String color = items.getYon ( );
                tintBackground ( color );
                //Toast.makeText(ctx, ""+color, Toast.LENGTH_SHORT).show();
                pref.edit().putString(items.getC(), items.getSaat()).apply();

            } else {

                left.setBackground ( ctx.getResources ( ).getDrawable ( R.drawable.hisse_left_bg ) );
                rght.setBackground ( ctx.getResources ( ).getDrawable ( R.drawable.hisse_rihgt_bg ) );
            }

        } else {

            boolean varmi = helper.isAdded(items.getC());

            if ( varmi ) {
                holder.icon.setImageResource ( R.drawable.btn_kaynak_ekle_active );
            } else {
                holder.icon.setImageResource ( R.drawable.btn_kaynak_ekle );
            }

            holder.sembol.setText ( items.getSembol ( ) );
            holder.name.setText ( items.getName ( ) );

            holder.icon.setOnClickListener ( new View.OnClickListener ( ) {
                @Override
                public void onClick ( View view ) {
                    boolean varmi = helper.isAdded(items.getC());
                    if ( varmi ) {
                        helper.removeItem(items.getC());
                        holder.icon.setImageResource ( R.drawable.btn_kaynak_ekle );

                    } else {
                        helper.addItem(items.getC(), items.getSaat());
                        holder.icon.setImageResource ( R.drawable.btn_kaynak_ekle_active );
                    }
                }
            } );
        }

        return row;
    }

    static class ViewHolder {
        MyTextView sembol;
        MyTextView degisim;
        MyTextView son;
        MyTextView saat;
        MyTextView name;
        ImageView icon;
    }


    private void tintBackground ( String color ) {
        int d = (int) Double.parseDouble(color);
        Log.i("Renk", "" + d);
        Resources res = ctx.getResources ( );
        Drawable[] drawables;
        Drawable[] drawable2;
        if (d == 1) {
            drawables = new Drawable[]{res.getDrawable ( R.drawable.hisse_left_bg_green ), res.getDrawable ( R.drawable.hisse_left_bg )};
            drawable2 = new Drawable[]{res.getDrawable ( R.drawable.hisse_rihgt_bg_green ), res.getDrawable ( R.drawable.hisse_rihgt_bg )};
        } else if (d == -1) {
            drawables = new Drawable[]{res.getDrawable ( R.drawable.hisse_left_bg_red ), res.getDrawable ( R.drawable.hisse_left_bg )};
            drawable2 = new Drawable[]{res.getDrawable ( R.drawable.hisse_rihgt_bg_red ), res.getDrawable ( R.drawable.hisse_rihgt_bg )};
        } else {
            drawables = new Drawable[]{res.getDrawable ( R.drawable.hisse_left_bg_gray ), res.getDrawable ( R.drawable.hisse_left_bg )};
            drawable2 = new Drawable[]{res.getDrawable ( R.drawable.hisse_rihgt_bg_gray ), res.getDrawable ( R.drawable.hisse_rihgt_bg )};
        }


        TransitionDrawable trans = new TransitionDrawable ( drawables );
        TransitionDrawable trans2 = new TransitionDrawable ( drawable2 );
        left.setBackground ( trans );
        rght.setBackground ( trans2 );
        trans.startTransition ( 500 );
        trans2.startTransition ( 500 );
    }
}
