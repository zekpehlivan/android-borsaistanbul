package com.borsaistanbul.mobil.adapters;


public class TahvilItems {
    private String basitEndeks;
    private String bilesikEndex;
    private String legacyCode;
    private String time;
    private String diffDays;
    private String dailyVolume;

    public TahvilItems(String basitEndeks, String bilesikEndex, String legacyCode, String time, String diffDays, String dailyVolume) {
        this.basitEndeks = basitEndeks;
        this.bilesikEndex = bilesikEndex;
        this.legacyCode = legacyCode;
        this.time = time;
        this.diffDays = diffDays;
        this.dailyVolume = dailyVolume;

    }

    public String getDailyVolume() {
        return dailyVolume;
    }

    public String getDiffDays() {
        return diffDays;
    }

    public String getBasitEndeks() {
        return basitEndeks;
    }

    public String getBilesikEndex() {
        return bilesikEndex;
    }

    public String getLegacyCode() {
        return legacyCode;
    }

    public String getTime() {
        if (time.length() > 0) {
            char[] s = time.toCharArray();
            String t = ":";
            StringBuilder newString = new StringBuilder(s.length);
            for (int i = 0; i < s.length; i++) {
                if (i % 2 == 1) {
                    if (i > 0) {
                        newString.append(s[i]).append(t);
                    }
                } else {

                    newString.append(s[i]);
                }
            }


            return newString.toString().substring(0, newString.length() - 1);
        } else {
            return time;
        }
    }
}
