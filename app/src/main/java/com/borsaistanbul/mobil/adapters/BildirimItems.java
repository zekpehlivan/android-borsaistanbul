package com.borsaistanbul.mobil.adapters;


public class BildirimItems {
    private String kod;
    private String saat;
    private String unvan;
    private String title;
    private String bildirimNo;


    public BildirimItems ( String kod, String saat, String unvan, String title, String bildirimNo ) {
        this.kod = kod;
        this.saat = saat;
        this.unvan = unvan;
        this.title = title;
        this.bildirimNo = bildirimNo;

    }

    public String getKod ( ) {
        return kod;
    }

    public String getSaat ( ) {
        return saat;
    }

    public String getUnvan ( ) {
        return unvan;
    }

    public String getTitle ( ) {
        return title;
    }

    public String getBildirimNo ( ) {
        return bildirimNo;
    }
}
