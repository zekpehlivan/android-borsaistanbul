package com.borsaistanbul.mobil.adapters;

import java.util.Locale;

@SuppressWarnings("WeakerAccess")
public class HisselerItems {
    private String sembol, degisim, son, saat, name, c, yon;
    private long dailyVolume;

    public HisselerItems(String sembol, String degisim, String son, String saat, String name, String c, String yon, Long dailyVolume) {
        this.sembol = sembol;
        this.degisim = degisim;
        this.son = son;
        this.saat = saat;
        this.name = name;
        this.c = c;
        this.yon = yon;
        this.dailyVolume = dailyVolume;
    }

    public long getDailyVolume() {

        return dailyVolume;


    }

    public String getSembol() {
        return sembol;
    }

    public String getDegisim() {
        return degisim;
    }

    public String getSon() {
        double t = Double.parseDouble ( son );
        return String.format ( Locale.getDefault ( ), "%.2f", t );
    }

    public String getSaat() {
        if (saat.length() > 0) {
            char[] s = saat.toCharArray();
            String t = ":";
            StringBuilder newString = new StringBuilder(s.length);
            for (int i = 0; i < s.length; i++) {
                if (i % 2 == 1) {
                    if (i > 0) {
                        newString.append(s[i]).append(t);
                    }
                } else {

                    newString.append(s[i]);
                }
            }


            return newString.toString().substring(0, newString.length() - 1);
        } else {
            return saat;
        }

    }

    public String getYon ( ) {
        return yon;
    }

    public String getName ( ) {
        return name;
    }

    public String getC ( ) {
        return c;
    }
}
