package com.borsaistanbul.mobil.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;

public class MenuAdapter extends ArrayAdapter <MenuItems> {
    private Context ctx;
    private int res;
    private ArrayList <MenuItems> list;

    public MenuAdapter ( Context ctx, int res, ArrayList <MenuItems> list ) {
        super ( ctx, res, list );
        this.ctx = ctx;
        this.res = res;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView ( int position, @Nullable View convertView, @NonNull ViewGroup parent ) {
        View row = convertView;
        ViewHolder holder;
        if ( row == null ) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService ( Context.LAYOUT_INFLATER_SERVICE );
            assert layoutInflater != null;
            row = layoutInflater.inflate ( res, parent, false );
            holder = new ViewHolder ( );
            holder.text = row.findViewById ( R.id.text );
            row.setTag ( holder );

        } else {
            holder = (ViewHolder) row.getTag ( );
        }
        MenuItems items = list.get ( position );
        holder.text.setText ( items.getMenu ( ) );
        return row;
    }

    static class ViewHolder {
        MyTextView text;
    }
}
