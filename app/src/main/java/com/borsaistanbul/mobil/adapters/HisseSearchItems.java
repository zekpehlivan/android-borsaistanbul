package com.borsaistanbul.mobil.adapters;

import java.util.Locale;

@SuppressWarnings("WeakerAccess")
public class HisseSearchItems {
    private String sembol, saat, name, c;
    private String ord;

    public HisseSearchItems(String sembol, String saat, String name, String c, String ord) {
        this.sembol = sembol;
        this.name = name;
        this.c = c;
        this.ord = ord;
        this.saat = saat;

    }

    public String getSembol() {
        return sembol;
    }

    public String getSaat() {
        return saat;
    }

    public String getOrd() {
        return ord;
    }

    public String getName() {
        return name;
    }

    public String getC() {
        return c;
    }
}
