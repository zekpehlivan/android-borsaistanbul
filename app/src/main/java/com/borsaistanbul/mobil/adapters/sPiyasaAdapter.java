package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;


public class sPiyasaAdapter extends ArrayAdapter<EndekslerItem> {
    private Context ctx;
    private int res;
    private ArrayList<EndekslerItem> list;
    private View left;
    private View rght;
    private SharedPreferences pref;

    public sPiyasaAdapter(Context ctx, int res, ArrayList<EndekslerItem> list) {
        super(ctx, res, list);
        this.ctx = ctx;
        this.res = res;
        this.list = list;
        pref = ctx.getSharedPreferences("borsa", Context.MODE_PRIVATE);

    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();


            holder.degisim = row.findViewById(R.id.degisim);
            holder.saat = row.findViewById(R.id.saat);
            holder.sembol = row.findViewById(R.id.sembol);
            holder.son = row.findViewById(R.id.son);
            // holder.desc = row.findViewById(R.id.desc);


            row.setTag(holder);
        } else {

            holder = (ViewHolder) row.getTag();

        }

        final EndekslerItem items = list.get(position);


        left = row.findViewById(R.id.hisse_left);
        rght = row.findViewById(R.id.hisse_right);


        holder.sembol.setText(aciklamaDuzelt(items.getLegacyCode()));
        holder.degisim.setText(items.getAsk());
        holder.saat.setText(items.getTime());
        holder.son.setText(items.getBid());
        // holder.desc.setText(aciklamaDuzelt(items.getLegacyCode()));

        if (items.getGunlukDegisimOran().contains("-")) {
            holder.son.setTextColor(ctx.getResources().getColor(R.color.textRedColor));
            holder.degisim.setTextColor(ctx.getResources().getColor(R.color.textRedColor));

        } else {

            holder.son.setTextColor(ctx.getResources().getColor(R.color.textGreenColor));
            holder.degisim.setTextColor(ctx.getResources().getColor(R.color.textGreenColor));
        }

        String time = pref.getString(items.getCode(), "");
        boolean ref = time.equals(items.getTime());

        if (!ref) {
            String color = items.getDegisimYonu();
            tintBackground(color);
            pref.edit().putString(items.getCode(), items.getTime()).apply();

        } else {

            left.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_left_bg));
            rght.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_rihgt_bg));
        }


        return row;
    }

    private String aciklamaDuzelt(String legacyCode) {
        boolean firstChar = legacyCode.substring(0, 1).contains("S");

        if (firstChar) {
            legacyCode = legacyCode.substring(1);
        }
        switch (legacyCode) {
          /*  case "SUSD":
                legacyCode = "Amerikan Doları";
                break;
            case "SEUR":
                legacyCode = "Euro";
                break;
            case "SSAR":
                legacyCode = "Suudi Arabistan Riyali";
                break;
            case "SCAD":
                legacyCode = "Kanada Doları";
                break;
            case "SCHF":
                legacyCode = "İsviçre Frangı";
                break;
            case "SGBP":
                legacyCode = "İngiliz Sterlini";
                break;
            case "SJPY":
                legacyCode = "Japon Yeni";
                break;*/
            case "XGLD":
                legacyCode = "Spot Altın";
                break;
            case "GCEYREK":
                legacyCode = "Çeyrek Altın";
                break;
            case "GYARIM":
                legacyCode = "Yarım Altın";
                break;
            case "CUM":
                legacyCode = "Cumhuriyet Altını";
                break;
            case "GATA":
                legacyCode = "Ata Lira";
                break;
            case "GBESLI":
                legacyCode = "Beşli Altın";
                break;
            case "GIKIBUCUK":
                legacyCode = "İkibuçuk Altın";
                break;
            case "GGREMSE":
                legacyCode = "Gremse Altın";
                break;
            case "G14BIL":
                legacyCode = "14 Ayar Bilezik";
                break;
            case "G18BIL":
                legacyCode = "18 Ayar Bilezik";
                break;
            case "G22BIL":
                legacyCode = "22 Ayar Bilezik";
                break;
            case "GZIYNET":
                legacyCode = "Ziynet Altın";
                break;
            case "EURGLD":
                legacyCode = "Külçe Altın (EUR)";
                break;
            case "USGLD":
                legacyCode = "Külçe Altın (USD)";
                break;
            case "USGLDKG":
                legacyCode = "Külçe Altın (KG)";
                break;
            case "GLD":
                legacyCode = "Gram Altın";
                break;
            case "XHGLD":
                legacyCode = "Has Altın";
                break;
            case "XSLV":
                legacyCode = "Gram Gümüş";
                break;


        }
        return legacyCode;
    }

    static class ViewHolder {
        MyTextView sembol;
        MyTextView degisim;
        MyTextView son;
        MyTextView saat;
        MyTextView desc;
    }


    private void tintBackground(String color) {
        int c = (int) Double.parseDouble(color);
        Log.i("Renk", "" + c);
        Resources res = ctx.getResources();
        Drawable[] drawables;
        Drawable[] drawable2;
        if (c == 1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_green), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_green), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else if (c == -1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_red), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_red), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_gray), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_gray), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        }


        TransitionDrawable trans = new TransitionDrawable(drawables);
        TransitionDrawable trans2 = new TransitionDrawable(drawable2);
        left.setBackground(trans);
        rght.setBackground(trans2);
        trans.startTransition(500);
        trans2.startTransition(500);
    }
}
