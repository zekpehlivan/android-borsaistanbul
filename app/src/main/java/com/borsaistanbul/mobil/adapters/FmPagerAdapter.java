package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.borsaistanbul.mobil.fragments.Bfy;
import com.borsaistanbul.mobil.fragments.Endeksler;
import com.borsaistanbul.mobil.fragments.HisseEndeks;
import com.borsaistanbul.mobil.activities.Opsiyonlar;
import com.borsaistanbul.mobil.fragments.OpsionGrup;
import com.borsaistanbul.mobil.fragments.Sertifikalar;
import com.borsaistanbul.mobil.activities.Sozlesmeler;
import com.borsaistanbul.mobil.fragments.SozlesmeGrup;
import com.borsaistanbul.mobil.fragments.Tahviller;
import com.borsaistanbul.mobil.fragments.Varant;


public class FmPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles = {"Endeksler ", "Paylar", "Varantlar", "Sertifikalar", "Borsa Yatırım Fonu (BYF)", "Vadeli İşlem Sözleşmeleri", "Opsiyonlar", "Tahviller"};
    // private String[] titles = {"Endeksler ", "Hisseler", "Varant", "Sertifika", "Opsiyonlar", "Vadeli\nSözleşmeler", "Tahviller", "Serbest Piyasalar", "Emtialar"};

    public FmPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        mContext = c;
        double skala = c.getResources().getDisplayMetrics().density;
    }

    @SuppressLint("Assert")
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Endeksler();
                break;
            case 1:
                fragment = new HisseEndeks();
                break;
            case 2:
                fragment = new Varant();
                break;
            case 3:
                fragment = new Sertifikalar();
                break;
            case 4:
                fragment = new Bfy();
                break;
            case 5:
                fragment = new SozlesmeGrup();
                break;
            case 6:
                fragment = new OpsionGrup();
                break;
            case 7:
                fragment = new Tahviller();
                break;
           /* case 8:
                fragment = new Emtialar();
                break;*/
        }

        Bundle b = new Bundle();
        b.putInt(
                "position", position
        );

        if (fragment != null) {
            fragment.setArguments(b);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (titles[position]);
    }
}
