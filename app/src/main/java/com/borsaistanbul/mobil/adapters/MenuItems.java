package com.borsaistanbul.mobil.adapters;

public class MenuItems {
    private String menu, id;

    public MenuItems ( String menu, String id ) {
        this.menu = menu;
        this.id = id;
    }

    public String getMenu ( ) {
        return menu;
    }

    public String getId ( ) {
        return id;
    }
}
