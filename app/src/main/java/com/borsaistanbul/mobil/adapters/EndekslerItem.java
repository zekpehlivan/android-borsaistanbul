package com.borsaistanbul.mobil.adapters;



public class EndekslerItem {
    private String gunlukDegisimOran, gunlukDegisim, code, last, desc, time, legacyCode, degisimYonu, ask, bid;
    private long dailyVolume;
    private int order;

    public EndekslerItem(String gunlukDegisimOran, String gunlukDegisim, String code, String last, String desc, String time, String legacyCode, String degisimYonu, String ask, String bid, Long dailyVolume, int order) {
        this.gunlukDegisimOran = gunlukDegisimOran;
        this.gunlukDegisim = gunlukDegisim;
        this.code = code;
        this.last = last;
        this.desc = desc;
        this.time = time;
        this.legacyCode = legacyCode;
        this.degisimYonu = degisimYonu;
        this.ask = ask;
        this.bid = bid;
        this.dailyVolume = dailyVolume;
        this.order = order;
    }

    public long getDailyVolume() {

        return dailyVolume;


    }

    public int getOrder() {
        return order;
    }

    public String getGunlukDegisimOran() {
        return gunlukDegisimOran;
    }

    public String getGunlukDegisim() {
        return gunlukDegisim;
    }

    public String getCode() {
        return code;
    }

    public String getLast() {
        return last;
    }

    public String getDesc() {
        return desc;
    }

    public String getTime() {
        if (time.length() > 0) {
            char[] s = time.toCharArray();
            String t = ":";
            StringBuilder newString = new StringBuilder(s.length);
            for (int i = 0; i < s.length; i++) {
                if (i % 2 == 1) {
                    if (i > 0) {
                        newString.append(s[i]).append(t);
                    }
                } else {

                    newString.append(s[i]);
                }
            }

            if (s.length > 0) {
                return newString.toString().substring(0, newString.length() - 1);

            } else {
                return time;
            }
        } else {
            return time;
        }

    }

    public String getLegacyCode() {
       return legacyCode.replace(".OANA.VIOP", "")
                .replace(".FANA.VIOP", "")
                .replace(".DFAN.VIOP", "")
                .replace(".EFAN.VIOP", "")
                .replace(".KFAN.VIOP", "")
                .replace(".EVAN.VIOP", "")
                .replace(".BFAN.VIOP", "")
                .replace(".ORAN.VIOP", "")
                .replace(".CFAN.VIOP", "")
                .replace(".MFAN.VIOP", "")
                .replace(".YEFA.VIOP", "")
                .replace(".DOAN.VIOP", "")
                .replace(".EOAN.VIOP", "");

    }

    public String getDegisimYonu() {
        return degisimYonu;
    }

    public String getAsk() {
        return ask;
    }

    public String getBid() {
        return bid;
    }
}
