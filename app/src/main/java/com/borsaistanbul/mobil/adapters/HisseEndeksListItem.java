package com.borsaistanbul.mobil.adapters;


public class HisseEndeksListItem {
    private String c, legacyCode, desc;

    public HisseEndeksListItem(String c, String legacyCode, String desc) {
        this.c = c;
        this.legacyCode = legacyCode;
        this.desc = desc;
    }

    public String getC() {
        return c;
    }

    public String getLegacyCode() {
        return legacyCode;
    }

    public String getDesc() {
        return desc;
    }
}
