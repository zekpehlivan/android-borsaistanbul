package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;


public class TahvilAdapter extends ArrayAdapter<TahvilItems> {
    private Context ctx;
    private int res;
    private ArrayList<TahvilItems> list;
    private View left;
    private View rght;
    private SharedPreferences pref;

    public TahvilAdapter(Context ctx, int res, ArrayList<TahvilItems> list) {
        super(ctx, res, list);
        this.ctx = ctx;
        this.res = res;
        this.list = list;
        pref = ctx.getSharedPreferences("borsa", Context.MODE_PRIVATE);

    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();


            holder.degisim = row.findViewById(R.id.degisim);
            holder.saat = row.findViewById(R.id.saat);
            holder.sembol = row.findViewById(R.id.sembol);
            holder.son = row.findViewById(R.id.son);


            row.setTag(holder);
        } else {

            holder = (ViewHolder) row.getTag();

        }

        final TahvilItems items = list.get(position);

        int dateDiff = Integer.parseInt(items.getDiffDays());
        if (dateDiff == 0) {
            left = row.findViewById(R.id.hisse_left);
            rght = row.findViewById(R.id.hisse_right);


            holder.sembol.setText(items.getLegacyCode());
            holder.degisim.setText(items.getBasitEndeks());
            holder.saat.setText(items.getTime());
            holder.son.setText(items.getBilesikEndex());


            holder.son.setTextColor(ctx.getResources().getColor(R.color.black));
            holder.degisim.setTextColor(ctx.getResources().getColor(R.color.black));


            left.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_left_bg));
            rght.setBackground(ctx.getResources().getDrawable(R.drawable.hisse_rihgt_bg));

        }


        return row;
    }

    static class ViewHolder {
        MyTextView sembol;
        MyTextView degisim;
        MyTextView son;
        MyTextView saat;

    }


    private void tintBackground(String color) {
        int c = (int) Double.parseDouble(color);
        Log.i("Renk", "" + c);
        Resources res = ctx.getResources();
        Drawable[] drawables;
        Drawable[] drawable2;
        if (c == 1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_green), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_green), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else if (c == -1) {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_red), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_red), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        } else {
            drawables = new Drawable[]{res.getDrawable(R.drawable.hisse_left_bg_gray), res.getDrawable(R.drawable.hisse_left_bg)};
            drawable2 = new Drawable[]{res.getDrawable(R.drawable.hisse_rihgt_bg_gray), res.getDrawable(R.drawable.hisse_rihgt_bg)};
        }


        TransitionDrawable trans = new TransitionDrawable(drawables);
        TransitionDrawable trans2 = new TransitionDrawable(drawable2);
        left.setBackground(trans);
        rght.setBackground(trans2);
        trans.startTransition(500);
        trans2.startTransition(500);
    }
}
