package com.borsaistanbul.mobil.adapters;

public class HistoryItems {
    private String d, o, h, l, c, v, a;

    public HistoryItems ( String d, String o, String h, String l, String c, String v, String a ) {
        this.d = d;
        this.o = o;
        this.h = h;
        this.l = l;
        this.c = c;
        this.v = v;
        this.a = a;
    }

    public String getD ( ) {
        return d;
    }

    public String getO ( ) {
        return o;
    }

    public String getH ( ) {
        return h;
    }

    public String getL ( ) {
        return l;
    }

    public String getC ( ) {
        return c;
    }

    public String getV ( ) {
        return v;
    }

    public String getA ( ) {
        return a;
    }
}
