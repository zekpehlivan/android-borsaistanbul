package com.borsaistanbul.mobil.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.borsaistanbul.mobil.R;
import com.borsaistanbul.mobil.helper.DbHelper;
import com.borsaistanbul.mobil.helper.MyTextView;

import java.util.ArrayList;
import java.util.List;


public class HisseSearchAdapter extends ArrayAdapter<HisseSearchItems> {
    private Context ctx;
    private int res;
    private ArrayList<HisseSearchItems> list;
    private DbHelper helper;
    private View left, rght;
    private SharedPreferences pref;
    LayoutInflater layoutInflater;
    private ArrayList<String> isAdded = new ArrayList<>();

    public HisseSearchAdapter(@NonNull Context context, int resource, ArrayList<HisseSearchItems> hisselerItems) {
        super(context, resource, hisselerItems);
        this.ctx = context;
        this.res = resource;
        this.list = hisselerItems;
        pref = ctx.getSharedPreferences("borsa", Context.MODE_PRIVATE);
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            row = layoutInflater.inflate(res, parent, false);
            holder = new ViewHolder();
            holder.icon = row.findViewById(R.id.btnEkle);


            holder.sembol = row.findViewById(R.id.sembol);
            holder.name = row.findViewById(R.id.name);


            row.setTag(holder);
        } else {

            holder = (ViewHolder) row.getTag();

        }

        final HisseSearchItems items = list.get(position);
        helper = new DbHelper(ctx);
        left = row.findViewById(R.id.hisse_left);
        rght = row.findViewById(R.id.hisse_right);
        boolean varmi = checkDb(items.getC());

        if (varmi) {
            holder.icon.setImageResource(R.drawable.btn_kaynak_ekle_active);
        } else {
            holder.icon.setImageResource(R.drawable.btn_kaynak_ekle);
        }

        holder.sembol.setText(items.getSembol());
        holder.name.setText(items.getName());

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean varmi = checkDb(items.getC());
                if (varmi) {
                    helper.removeItem(items.getC());
                    holder.icon.setImageResource(R.drawable.btn_kaynak_ekle);

                } else {
                    helper.addItem(items.getC(), items.getSaat());
                    holder.icon.setImageResource(R.drawable.btn_kaynak_ekle_active);
                }
            }
        });


        return row;
    }

    static class ViewHolder {
        MyTextView sembol;
        MyTextView saat;
        MyTextView name;
        ImageView icon;
    }

    private boolean checkDb(String cCode) {
        if (isAdded.size() > 0) {
            isAdded.clear();
        }
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.rawQuery("Select * from list", null);


        while (c.moveToNext()) {
            isAdded.add(c.getString(c.getColumnIndex("kod")));

        }
        db.close();
        c.close();

        return isAdded.contains(cCode);
    }
}


